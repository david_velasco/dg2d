import numpy as np
import matplotlib
matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import math
import sys
import os

#shape = [int(sys.argv[2]),int(sys.argv[2])]
fig, axs = plt.subplots(2,2)
fig2, axs2 = plt.subplots(2,2)
fig3, axs3 = plt.subplots(2,2)
var = sys.argv[1]
nx=30
ny=30
mx=2
my=2
nvar=4
size=nx*ny*my*mx*nvar
m=np.fromfile("dudt.dat",dtype=np.float64,count=size).reshape(nvar,nx,ny,mx,my)
m2=np.fromfile("/home/ics/velasco/DG2D/dudt.dat",dtype=np.float64).reshape(my,mx,ny,nx,nvar)

for imod in range(mx):
    for jmod in range(my):
        sliced = (np.fabs((m[var,0:nx,0:ny,imod,jmod]-np.transpose(m2[jmod,imod,0:ny,0:nx,var]))/m[var,0:nx,0:ny,imod,jmod]))
        im=axs[imod,jmod].imshow(sliced,interpolation="nearest",origin="lower")
        axs[imod,jmod].set_title("max = "+str(sliced.max())+" min = "+str(sliced.min()))
        divider = make_axes_locatable(axs[imod,jmod])
        cax = divider.append_axes("right", size="20%", pad=0.05)
        fig.colorbar(im, cax=cax)
        
        im=axs2[imod,jmod].imshow(np.transpose(m2[jmod,imod,0:ny,0:nx,var]),interpolation="nearest",origin="lower")
        divider = make_axes_locatable(axs2[imod,jmod])
        cax = divider.append_axes("right", size="20%", pad=0.05)
        fig2.colorbar(im, cax=cax)

        im=axs3[imod,jmod].imshow(m[var,0:nx,0:ny,imod,jmod],interpolation="nearest",origin="lower")
        divider = make_axes_locatable(axs3[imod,jmod])
        cax = divider.append_axes("right", size="20%", pad=0.05)
        fig3.colorbar(im, cax=cax)
        #axs2[imod,jmod].axis([0,30,0,30])
        #print m[3,0:nx,0:ny,imod,jmod]
plt.show()


