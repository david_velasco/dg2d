import numpy as np
from matplotlib import pyplot as plt
import sys
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as manimation
import os

def plot_curves(curve1, curve2, size_x, picture_name):
    """ function eats .dat files """
    size_y=size_x
    shape = [int(size_x),int(size_y)]
    data_1 = np.loadtxt(curve1,dtype=str)
    x_1 = np.array(data_1[:,0]).reshape(shape,order='C')
    y_1 = np.array(data_1[:,1]).reshape(shape,order='C')
    f_1 = np.array(data_1[:,2]).reshape(shape,order='C')
    print x_1
    print f_1
    # plt.plot(x_end,rho_end, label='curve 1')
    a = plt.contour(x_1, y_1, f_1,75, label='curve 1')
   # print np.max(f_1)
    #data_2 = np.loadtxt(curve2)
    #x_2 = data_2[:,0].reshape(shape,order='C')
    #y_2 = data_2[:,1].reshape(shape,order='C')
    #rho_2 = data_2[:,2].reshape(shape,order='C')
    #b = plt.contour(x_2,y_2,rho_2,15, label='curve 2')
    # b = plt.contour(xcoords, ycoords, step)
    #print np.max(rho_2)
    #plt.clabel(a, inline=1, fontsize=10)
    #plt.clabel(b, inline=1, fontsize=10)
    #plt.legend()
    
    #cbar = plt.colorbar(b)
    # ymax = max(np.max(rho_end1),np.max(rho_end))
    # ymin = min(np.min(rho_end),np.min(rho_end1))

    # xmax = max(np.max(x_end1),np.max(x_end))
    # xmin = min(np.min(x_end),np.min(x_end1))
    
    #print xmax, xmin, ymax, ymin
    #plt.axis([0,1/6.,0,1])

    plt.axes().set_aspect('equal')
    plt.savefig(str(picture_name)+'.png', dpi=300) 

def plot_image(curve1,curve2,size_x,picture_name):
    size_y=size_x
    shape = [int(size_x),int(size_y)]
    data_1 = np.loadtxt(curve1)
    x_1 = data_1[:,0].reshape(shape,order='C')
    y_1 = data_1[:,1].reshape(shape,order='C')
    f_1 = data_1[:,2].reshape(shape,order='C')
    # plt.plot(x_end,rho_end, label='curve 1')

    plt.imshow(f_1,interpolation='none')

    plt.axes().set_aspect('equal')
    plt.savefig(str(picture_name)+'.png') 

def get_image(snapfile,size_x):
    print snapfile
    size_y=size_x
    shape = [int(size_x),int(size_y)]
    data_1 = np.loadtxt(snapfile,dtype=str)
    x_1 = np.array(data_1[:,0]).reshape(shape,order='C')
    y_1 = np.array(data_1[:,1]).reshape(shape,order='C')
    f_1 = np.array(data_1[:,2]).reshape(shape,order='C')
    print f_1.shape
    print np.amin(f_1.astype(np.float))
    print np.amax(f_1.astype(np.float))
    #a = plt.imshow(f_1,interpolation='sinc')

    return x_1,y_1,f_1
    
def make_movie(folder_path, size_x,name):
    # list files in folder alphabetically
    list_of_snapshots = [f for f in os.listdir(folder_path) if f != '.DS_Store']
    # pass file to plot_image
    movie = []
    for snap in list_of_snapshots:
        movie.append(get_image(folder_path+snap,size_x))
    # output film

    FFMpegWriter = manimation.writers['ffmpeg']
    metadata = dict(title='Movie Test', artist='Matplotlib',
            comment='Movie support!')
    writer = FFMpegWriter(fps=5, metadata=metadata)
    fig = plt.figure()
    l, = plt.plot([], [], 'k-o')
    ax = fig.gca()
    ax.set_autoscale_on(False)
    with writer.saving(fig, name+".mp4", 100):
        for step in movie:
            #plt.imshow(step[2])
            try:
                plt.xlim([0,1])
                plt.ylim([0,1])
                plt.axes().set_aspect('equal','box')
                plt.contour(step[0],step[1],step[2],15)
                writer.grab_frame()
                plt.clf()
            except:
                continue

def plot_slice(curve1, size_x, picture_name):
    """ function eats .dat files """
    size_y=size_x
    shape = [int(size_x),int(size_y)]
    data_1 = np.loadtxt(curve1)
    x_1 = data_1[:,0].reshape(shape,order='C')
    y_1 = data_1[:,1].reshape(shape,order='C')
    rho = data_1[:,2].reshape(shape,order='C')
    u = data_1[:,3].reshape(shape,order='C')
    v = data_1[:,4].reshape(shape,order='C')
    p = data_1[:,5].reshape(shape,order='C')
    mid_pt = int(int(size_x)/2.)

    plt.plot(x_1[:,mid_pt],rho[mid_pt,:],label='rho')
    plt.plot(x_1[:,mid_pt],u[mid_pt,:],label='u')
    plt.plot(x_1[:,mid_pt],v[mid_pt,:],label='v')
    plt.plot(x_1[:,mid_pt],p[mid_pt,:],label='p')

    #plt.clabel(a, inline=1, fontsize=10)
    #plt.clabel(b, inline=1, fontsize=10)
    plt.legend()
    #cbar = plt.colorbar(b)
    #plt.axes().set_aspect('equal')
    plt.savefig(str(picture_name)+'.png', dpi=300) 
    plt.clf()

    plt.plot(y_1[mid_pt,:],rho[:,mid_pt],label='rho')
    plt.plot(y_1[mid_pt,:],u[:,mid_pt],label='u')
    plt.plot(y_1[mid_pt,:],v[:,mid_pt],label='v')
    plt.plot(y_1[mid_pt,:],p[:,mid_pt],label='p')
    plt.legend()
    #cbar = plt.colorbar(b)
    #plt.axes().set_aspect('equal')
    plt.savefig(str(picture_name)+'_trans.png', dpi=300) 
    plt.clf()

def plot_slices(folder_path, size_x, picture_name):
    list_of_snapshots = [f for f in os.listdir(folder_path) if f != '.DS_Store']
    for a in range(len(list_of_snapshots)):
        plot_slice(folder_path+'/'+list_of_snapshots[a],size_x,picture_name+str(a))

def slices_movie(folder_path, size_x, name):
    list_of_snapshots = [f for f in os.listdir(folder_path) if f != '.DS_Store']
    
    FFMpegWriter = manimation.writers['ffmpeg']
    metadata = dict(title='Movie Test', artist='Matplotlib',
            comment='Movie support!')
    writer = FFMpegWriter(fps=7, metadata=metadata)
    fig = plt.figure()
    l, = plt.plot([], [], 'k-o')
    ax = fig.gca()
    ax.set_autoscale_on(False)
    with writer.saving(fig, name+"_slices.mp4", 100):
        for a in range(len(list_of_snapshots)):
            size_y=size_x
            shape = [int(size_x),int(size_y)]
            data_1 = np.loadtxt(folder_path+'/'+list_of_snapshots[a])
            x_1 = data_1[:,0].reshape(shape,order='C')
            y_1 = data_1[:,1].reshape(shape,order='C')
            rho = data_1[:,2].reshape(shape,order='C')
            u = data_1[:,3].reshape(shape,order='C')
            v = data_1[:,4].reshape(shape,order='C')
            p = data_1[:,5].reshape(shape,order='C')
            mid_pt = int(int(size_x)/2.)
            plt.ylim([-0.15,1.15])
            plt.plot(x_1[:,mid_pt],rho[mid_pt,:],label='rho')
            plt.plot(x_1[:,mid_pt],u[mid_pt,:],label='u')
            plt.plot(x_1[:,mid_pt],v[mid_pt,:],label='v')
            plt.plot(x_1[:,mid_pt],p[mid_pt,:],label='p')

            plt.legend()

            writer.grab_frame()
            plt.clf()



if __name__=='__main__':
    #plot_curves(sys.argv[1],sys.argv[2],sys.argv[3], sys.argv[4])
    #plot_image(sys.argv[1],sys.argv[2],sys.argv[3], sys.argv[4])
    #plot_slice(sys.argv[1],sys.argv[2],sys.argv[3])
    #plot_slices(sys.argv[1],sys.argv[2],sys.argv[3])
    
    make_movie(sys.argv[1],sys.argv[2],sys.argv[3])
    slices_movie(sys.argv[1],sys.argv[2],sys.argv[3])

