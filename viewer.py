import numpy as np
#import matplotlib
#matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpl_toolkits.axes_grid1 import make_axes_locatable
import math
import sys
import os
from optparse import OptionParser

#Command line parser/////////////////////////////////////////
parser = OptionParser()
parser.add_option("-o", "--output", action="store", type="int", dest="o", default ="0")
parser.add_option("-d", "--dir",    action="store", type="string", dest="d", default="outputs")
options, args = parser.parse_args()
output = options.o
folder = options.d
try:
    nx,ny,m,nvar = np.loadtxt(folder+"/Descriptor.dat",usecols=(0,1,2,3),dtype=int,unpack=True)
    lx,ly,gamma = np.loadtxt(folder+"/Descriptor.dat",usecols=(4,5,6),dtype=float,unpack=True)
    print nx, ny, m, nvar, lx, ly, gamma
except:
    print "Couldn't open descriptor"
    exit()
fig = plt.figure()
ax1 = plt.subplot2grid((2,2), (0,0), rowspan=2)
ax2 = plt.subplot2grid((2,2), (0,1))
ax3 = plt.subplot2grid((2,2), (1,1))

var = 0
size=nx*ny*m*m*nvar
n=output

x = np.arange(ny)
y = np.arange(nx)
rho0 = np.zeros((ny,nx))
p0 = np.zeros((ny,nx))
im=0
jm=0
field="rho"

def visualize(i):
    global n,im,jm,field
    n=i
    name=folder+"/modes"+str(n).zfill(5)+".dat"
    try:
        Ma=np.fromfile(name,dtype=np.float64,count=size).reshape(nvar,m,m,ny,nx)
    except:
        print "Couldn't open file: "+name
        return
    if field=='rho':                   
        ma = Ma[0,jm,im,:,:]
    elif field=='vx':
        ma = Ma[1,jm,im,:,:]/Ma[0,jm,im,:,:]
    elif field=='vy':
        ma = Ma[2,jm,im,:,:]/Ma[0,jm,im,:,:] 
    elif field=='p':
        rho = Ma[0,jm,im,:,:] 
        vx  = Ma[1,jm,im,:,:]/rho               
        vy  = Ma[2,jm,im,:,:]/rho
        ma =  (gamma-1.0)*(Ma[3,jm,im,:,:]-0.5*rho*(vx*vx+vy*vy));            
        
    image=(ax1.imshow(ma,interpolation="nearest",origin="lower",cmap="gnuplot"))
    ax2.cla()
    ax2.plot(x,ma[ny/2,:],"b")
    ax2.set_xlim([0,nx])
    ax3.cla()
    ax3.plot(y,ma[:,nx/2],"r")
    ax3.set_xlim([0,ny])
    ax1.set_title("Output "+str(n)+" "+field+": min = "+str(round(ma.min(),3))+" max = "+str(round(ma.max(),3)),fontsize=10)
    
    return image   
    
def change_file(event):
    sys.stdout.flush()
    global n,cbar,field
    if event.key=='right':
        n+=1
    elif event.key=='left':
        if n>0:
            n-=1
    elif event.key=='r':
        field='rho'
    elif event.key=='u':
        field='vx'
    elif event.key=='v':
        field='vy'
    elif event.key=='p':
        field='p'
    else:
        return
    image=visualize(n)
    cbar.update_bruteforce(image)
    fig.canvas.draw_idle()

image=visualize(n)    
cbar=fig.colorbar(image,ax=ax1,orientation="horizontal")

fig.canvas.mpl_connect('key_press_event', change_file)   
plt.show()



