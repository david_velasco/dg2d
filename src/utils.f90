subroutine l1norm(u_analytical,u_numerical,nx,ny,m,l1error)
        use parameters_dg_2d
        implicit none
        real(kind=8),dimension(1:m,1:m,1:nx,1:ny,1:nvar)::u_analytical, u_numerical
        real(kind=8),dimension(1:nvar)::l1error

        integer::i,j,ii,jj

        do i = 1,nx
           do j=1,ny
              do ii = 1,m
                  do jj = 1,m 
                        l1error(1:nvar)+= abs(u_analytical(ii,jj,i,j,1:nvar)- u_numerical(ii,jj,i,j,1:nvar))
                  end do
              end do 
           end do
        end do
        
        l1error = l1error/(nx*ny*m*m)

end subroutine l1norm
