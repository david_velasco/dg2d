#include <stdio.h>
#include "cuda.h"
#include "global.h"
#include <assert.h>

#define BLOCK 256
#define HLLC //Activate HLLC fluxes, default are LLF
//#define SRC //Activate source
//#define LASRC 0.001
//#define CP
//#define TEST
#define RHO0 1E-10
#define P0 1E-10
#define LIMIT //Abilitate limiters
#define HIO //Activate high-order limiter
#define PRC 1E-2 //Percentage threshold on the high-order limiter
//#define LOW_ALPHA //Activate diffusive bound for the high-order limiter
//#define PP //Activate Positivity preserving limiter
#define NR 10 //number of Newton-Raphson iterations on the Positivity preserving limiter

__constant__ double sqrt_mod[5];
__constant__ double xquad[5];
__constant__ double yquad[5];
__constant__ double wxquad[5];
__constant__ double wyquad[5];
__constant__ double xgll[6];
__constant__ double ygll[6];
__constant__ double wxgll[6];
__constant__ double wygll[6];

__device__  double legendre(double x, int n, int sq){
  double legendre;
  x=min(max(x,-1.0),1.0);
  switch (n) {
  case 0:
    legendre=1.;
    break;
  case 1:
    legendre=x;
    break;
  case 2:
    legendre=0.5*(3.0*x*x-1.0);
    break;
  case 3:
    legendre=(2.5*x*x*x-1.5*x);
    break;
  case 4:
    legendre=0.125*(35.0*x*x*x*x-30.0*x*x+3.0);
    break;
  case 5:
    legendre=0.125*(63.0*pow(x,5)-70.0*pow(x,3)+15.0*x);
    break;
  case 6:
    legendre=1.0/16.0*(231.0*pow(x,6)-315.0*pow(x,4)+105.0*pow(x,2)-5.0);
    break;
  }
  if(sq==1)
    legendre *= sqrt_mod[n];
  return legendre;
}

__device__  double legendre_prime(double x, int n, int sq){
  double legendre_prime;
  x=min(max(x,-1.0),1.0);
  switch (n) {
  case 0:
    legendre_prime=0.0;
    break;
  case 1:
    legendre_prime=1.0;
    break;
  case 2:
    legendre_prime=3*x;
    break;
  case 3:
    legendre_prime=0.5*(15.0*x*x-3.0);
    break;
  case 4:
    legendre_prime=0.125*(140.0*x*x*x-60.0*x);
    break;
  case 5:
    legendre_prime=0.125*(315.0*pow(x,4)-210.0*pow(x,2)+15.0);
    break;
  case 6:
    legendre_prime=1.0/16.0*(1386.0*pow(x,5)-1260.0*pow(x,3)+210.0*x);
    break;
  }
  if(sq==1)
    legendre_prime *= sqrt_mod[n];
  return legendre_prime;
}

__device__  double minmod(double x, double y, double z){
  double s;
  s=copysign(1.0,x);
  if(copysign(1.0,y) == s && copysign(1.0,z) == s)
    return (double)s*min(fabs(x),min(fabs(y),fabs(z)));
  else
     return 0.0;
}

__device__ int BC(int index, int size, int bc){
  if (bc == 1){//periodic
    if (index == -1) 
      index = size-1;
    else if (index == size)
      index = 0;
  }
  else if (bc == 2 || bc == 3){//transmissive or reflective
    if (index == -1) 
      index++;
    else if (index == size)
      index--;
  }
  return index;
}

__device__ double limiting(double* u,int ic,int im,int ip,int jm,int jp,int in,int jn,int m,int b){
  double d_l_x, d_l_y, d_r_x, d_r_y;
  double coeff_i,coeff_j;
  double u_lim;
  int mode;
  u_lim = u[(in+jn*m)*b+ic];
  if(jn > 0){
#ifndef LOW_ALPHA
    coeff_j = sqrt((2.0*double(jn)-1.0)/(2.0*double(jn)+1.0));
#else
    coeff_j = 0.5/sqrt(4.0*double(jn*jn)-1.0);
#endif
    mode = (in+(jn-1)*m)*b;
    d_r_y = coeff_j*(u[mode+jp]-u[mode+ic]);
    d_l_y = coeff_j*(u[mode+ic]-u[mode+jm]);
    u_lim = minmod(u_lim,d_r_y,d_l_y);
  }
  if(in > 0){
#ifndef LOW_ALPHA
    coeff_i = sqrt((2.0*double(in)-1.0)/(2.0*double(in)+1.0));
#else
    coeff_i = 0.5/sqrt(4.0*double(in*in)-1.0);
#endif
    mode = ((in-1)+jn*m)*b;
    d_r_x = coeff_i*(u[mode+ip]-u[mode+ic]);
    d_l_x = coeff_i*(u[mode+ic]-u[mode+im]);            
    u_lim = minmod(u_lim,d_r_x,d_l_x);
  }
  return u_lim;
}

__device__ double solve_for_t(double rho, double mx, double my, double e, double rhoa, double mxa, double mya, double ea, double gamma, double eps){
  double a, b, c, d, t, t1, t2;
  int i, iter=NR;
  a = 2.0*(rho-rhoa)*(e-ea) - (mx-mxa)*(mx-mxa) - (my-mya)*(my-mya);
  b = 2.0*(rho-rhoa)*(ea-eps/(gamma-1)) + 2.0*rhoa*(e-ea) - 2.0*(mxa*(mx-mxa)+mya*(my-mya));
  c = 2.0*rhoa*ea - (mxa*mxa+mya*mya) - 2.0*eps*rhoa/(gamma-1.0);
  d = sqrt(fabs(b*b-4.0*a*c));
  if( b >= 0.0){
    t1 = 0.5*(-b-d)/a;
    t2 = 2*c/(-b-d);
  }
  else{
    t1 = 2*c/(-b+d);
    t2 =0.5*(-b+d)/a;
  }
  if( t1 >= 0.0 && t1 <= 1.0 )
    t = t1;
  else if( t2 >= 0.0 && t2 <= 1.0)
    t = t2;
  else
    t = 0.0;
  if (t==0.0 || t==1.0)
    return t;
  for(i=0;i<iter;i++)
    t = t - (a*t*t+b*t+c)/(2*a*t+b); 
   return t;
}

__global__ void get_modes_from_nodes(double* nodes, double* modes, int m, int ny, int nx, int nvar){

  int id, ic, jc, im, jm, var;
  int iq, jq, cid;
  int a = nx;
  int b = ny*a;
  int c = m*b;
  int d = m*c;
  int size = nvar*d;
  double val=0.0;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  var = id/d;
  ic  = id - var*d;
  jm  = ic/c;
  ic -= jm*c;
  im  = ic/b;
  ic -= im*b;
  jc =  ic/a;
  ic -= jc*a;
  cid = ic + jc*a + var*d;
  
  if( id < size ){
    for( iq=0; iq < m; iq++){
       for( jq=0; jq < m; jq++)
	 val += 0.25*nodes[iq*b+jq*c+cid]*legendre(xquad[iq],im,1)*legendre(yquad[jq],jm,1)
	   * wxquad[iq]*wyquad[jq];
    }
    modes[id] = val;
  }
}

__global__ void get_nodes_from_modes(double* modes, double* nodes, int m, int ny, int nx, int nvar){

  int id, ic, jc, iq, jq, var;
  int im, jm, cid;
  int a = nx;
  int b = ny*a;
  int c = m*b;
  int d = m*c;
  int size = nvar*d;
  double val=0.0;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  var = id/d;
  ic  = id - var*d;
  jq  = ic/c;
  ic -= jq*c;
  iq  = ic/b;
  ic -= iq*b;
  jc =  ic/a;
  ic -= jc*a;
  cid = ic + jc*a + var*d;
  
  if( id < size ){
    for( im=0; im < m; im++){
       for( jm=0; jm < m; jm++)
	 val += modes[im*b+jm*c+cid]
	   *legendre(xquad[iq],im,1)*legendre(yquad[jq],jm,1);
    }
    nodes[id] = val;
   }
}

__global__ void compute_primitive(double* u, double* w, double gamma, int usize, int size){
  int id; 
  double rho,vx,vy,rho_l;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  if( id < size ){
    rho = w[id] = u[id];
    rho_l = max(rho,rho);
    vx  = w[id+usize] = u[id+usize]/rho_l;
    vy  = w[id+usize*2] = u[id+usize*2]/rho_l;
    w[id+usize*3] = (gamma-1.0)*(u[id+usize*3] - 0.5*(vx*vx+vy*vy)*rho_l);
  }
}

__global__ void compute_conservative(double* w, double* u, double gamma, int usize, int size){
  int id; 
  double rho,vx,vy;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  if( id < size ){
    rho = u[id] = w[id];
    vx = w[id+usize];
    vy = w[id+usize*2];
    u[id+usize]   = rho*vx;
    u[id+usize*2] = rho*vy;
    u[id+usize*3] = w[id+usize*3]/(gamma-1.0) + 0.5*rho*(vx*vx+vy*vy);
  }
}

__global__ void cons_to_prim(double* du,double* dw,double gamma,int m,int ny,int nx,int usize,int size){
  int id, cid, ic, jc, mo; 
  double rho,vx,vy,drho,dmx,dmy,rho_l;
  int a = nx;
  int b = ny*a;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  mo  = id/b;
  ic  = id - mo*b;
  jc  = ic/a;
  ic -= jc*a;
  cid = ic + jc*a;
  if( id < size ){
    id += b;
    drho = dw[id] = du[id];
    rho = dw[cid];
    rho_l = max(rho,rho);
    vx = dw[cid+usize];
    vy = dw[cid+usize*2];
    dmx = du[id+usize];
    dmy = du[id+usize*2];
    dw[id+usize]   = (dmx-drho*vx)/rho_l;
    dw[id+usize*2] = (dmy-drho*vy)/rho_l;
    dw[id+usize*3] = (gamma-1.0)*(0.5*drho*(vx*vx+vy*vy)-vx*dmx-vy*dmy + du[id+usize*3]);
  }
}

__global__ void prim_to_cons(double* dw,double* du,double gamma,int m,int ny,int nx,int usize,int size){
  int id, cid, ic, jc, mo; 
  double rho,vx,vy,drho,dvx,dvy,rho_l;
  int a = nx;
  int b = ny*a;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  mo = id/b;
  ic  = id - mo*b;
  jc  = ic/a;
  ic -= jc*a;
  cid = ic + jc*a;
  if( id < size ){
    id += b;
    drho = du[id] = dw[id];
    rho = dw[cid];
    rho_l = max(rho,rho);
    vx = dw[cid+usize];
    vy = dw[cid+usize*2];
    dvx = dw[id+usize];
    dvy = dw[id+usize*2];
    du[id+usize]   = vx*drho+rho_l*dvx;
    du[id+usize*2] = vy*drho+rho_l*dvy;
    du[id+usize*3] = 0.5*drho*(vx*vx+vy*vy)+rho_l*(vx*dvx+vy*dvy)+dw[id+usize*3]/(gamma-1.0);
  }
}
 
__global__ void compute_flux(double* u, double* w, double* flux1, double* flux2, int size){
  int id; 
  double rhovx,rhovy,e,vx,vy,p;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  if( id < size ){
    vx  = w[id+size];  
    vy  = w[id+size*2];
    p   = w[id+size*3];  
    rhovx = u[id+size];  
    rhovy = u[id+size*2];
    e = u[id+size*3];
    flux1[id] = rhovx;
    flux2[id] = rhovy;
    flux1[id+size] = rhovx*vx+p;
    flux2[id+size] = rhovy*vx;
    flux1[id+size*2] = rhovx*vy;
    flux2[id+size*2] = rhovy*vy+p;
    flux1[id+size*3] = vx*(e+p);
    flux2[id+size*3] = vy*(e+p);
   }
}

__global__ void flux_vol (double* f_vol, double* f_q1, double* f_q2, double invdx, double invdy, int m, int ny, int nx, int nvar){
  int id, ic, jc, iq, jq, va;
  int im, jm, cid;
  int a = nx;
  int b = ny*a;
  int c = m*b;
  int d = m*c;
  int size = nvar*d;
  double val1,val2;
  val1=val2=0.0;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  va = id/d;
  ic  = id - va*d;
  jm  = ic/c;
  ic -= jm*c;
  im  = ic/b;
  ic -= im*b;
  jc =  ic/a;
  ic -= jc*a;
  cid = ic + jc*a + va*d;
  if( id < size ){
    for( iq=0; iq < m; iq++){
      for( jq=0; jq < m; jq++){
	val1 += f_q1[iq*b+jq*c+cid]*
	  legendre_prime(xquad[iq],im,1)*wxquad[iq]*
	  legendre(yquad[jq],jm,1)*wyquad[jq];
	val2 += f_q2[iq*b+jq*c+cid]* 
	  legendre_prime(yquad[jq],jm,1)*wyquad[jq]*
	  legendre(xquad[iq],im,1)*wxquad[iq];
      }
    }
    f_vol[id] = val1*invdx+val2*invdy;
  }
}

__global__ void compute_min_dt(double* w, double* Dt, double gamma, double cfl, double dx, double dy, int m, int usize, int size){
  int id, jump; 
  double dt,dt_min,cs,constant;
  __shared__ double mins[BLOCK];
  id = threadIdx.x;
  if(id < size){
     constant = cfl/double(2*m-1);
#ifdef LIMIT
#ifdef PP
    constant = cfl*min(1./double(2*m-1),0.5*wxgll[0]);
#endif
#endif
    cs=sqrt(gamma*max(w[id+usize*3],P0)/max(w[id],RHO0));
    dt_min = constant/((fabs(w[id+usize])+cs)/dx + (fabs(w[id+usize*2])+cs)/dy);
    for (id = threadIdx.x+blockDim.x; id < size; id += blockDim.x){ //This is implemented considering only one block in the reduction launch.
      cs=sqrt(gamma*max(w[id+usize*3],P0)/max(w[id],RHO0));
      dt = constant/((fabs(w[id+usize])+cs)/dx + (fabs(w[id+usize*2])+cs)/dy);
      dt_min=min(dt,dt_min);
    }
    mins[threadIdx.x] = dt_min;
  }
  __syncthreads();
  for(jump = blockDim.x/2; jump > 0; jump >>= 1){
    if( threadIdx.x < jump )
      mins[threadIdx.x]=min(mins[threadIdx.x],mins[threadIdx.x+jump]);
    __syncthreads();
  }
  if(threadIdx.x == 0)
    Dt[0] = mins[0]; 
}

__global__ void compute_dt_grav(double* w, double* Dt, double* grad, double gamma, int size){
  int id, jump; 
  double dt,dt_min,cs,constant,gradx,grady;
  __shared__ double mins[BLOCK];
  id = threadIdx.x;
  if(id < size){
    constant = 1/sqrt(2*gamma*(gamma-1));
    cs=sqrt(gamma*max(w[id+size*3],P0)/max(w[id],RHO0));
    gradx = grad[id];
    grady = grad[id+size];
    dt_min = constant*cs/sqrt(gradx*gradx+grady*grady);
    for (id = threadIdx.x+blockDim.x; id < size; id += blockDim.x){ //This is implemented considering only one block in the reduction launch.
      cs=sqrt(gamma*max(w[id+size*3],P0)/max(w[id],RHO0));
      gradx = grad[id];
      grady = grad[id+size];
      dt = constant*cs/sqrt(gradx*gradx+grady*grady);
      dt_min=min(dt,dt_min);
    }
    mins[threadIdx.x] = dt_min;
  }
  __syncthreads();
  for(jump = blockDim.x/2; jump > 0; jump >>= 1){
    if( threadIdx.x < jump )
      mins[threadIdx.x]=min(mins[threadIdx.x],mins[threadIdx.x+jump]);
    __syncthreads();
  }
  if(threadIdx.x == 0)
    Dt[0] = mins[0]; 
}

__global__ void compute_faces(double* ufaces, double* delta_u, int m, int ny, int nx, int nvar){
  int id, va, ic, jc, im, jm, q, cid, lid, rid, bid, tid;
  double shudl[5],shudr[5],shudb[5],shudt[5];
  double chsi_m = -1, chsi_p = 1, du;
  int a = nx;
  int b = ny*a;
  int c = m*b;
  int d = m*c;
  int size = nvar*b;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  va = id/b;
  ic = id - va*b;
  jc = ic/a;
  ic -= jc*a;
  cid = ic + jc*a + va*d;
  d = 4*c;
  lid = ic + jc*a + va*d;
  rid = lid + c;
  bid = rid + c;
  tid = bid + c;
  if( id < size ){
    for (q=0;q<m;q++)
      shudb[q] = shudt[q] = shudl[q] = shudr[q] = 0.0;
    for (im=0;im<m;im++){ 
      for (jm=0;jm<m;jm++){
	du = delta_u[im*b+jm*c+cid];
	for (q=0;q<m;q++){
	  shudl[q] += du*legendre(chsi_m,im,1)*legendre(yquad[q],jm,1);
	  shudr[q] += du*legendre(chsi_p,im,1)*legendre(yquad[q],jm,1);
	  shudb[q] += du*legendre(chsi_m,jm,1)*legendre(xquad[q],im,1);
	  shudt[q] += du*legendre(chsi_p,jm,1)*legendre(xquad[q],im,1);
	}
      }
    }
    for (q=0;q<m;q++){
      ufaces[lid+q*b]=shudl[q];
      ufaces[rid+q*b]=shudr[q];
      ufaces[bid+q*b]=shudb[q];
      ufaces[tid+q*b]=shudt[q];
    }
  }
}

__global__ void compute_GxGLL(double* nodesX, double* nodesY, double* modes, int k, int m, int ny, int nx, int nvar){
  int id, va, ic, jc, gl, gll, iq, jq, cid;
  int a = nx;
  int b = ny*a; 
  int c = m*b; 
  int d = k*c;
  int size = d*nvar;
  double valx,valy,u;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  va = id/d;
  ic  = id - va*d;
  gll = ic/c;
  ic -= gll*c;
  gl  = ic/b;
  ic -= gl*b;
  jc  = ic/a;
  ic -= jc*a;

  c = m*b;
  d = m*c;
  cid = ic + jc*a + va*d;
 
  if( id < size ){
    valx=valy=0.0;
    for(iq=0;iq<m;iq++){
      for(jq=0;jq<m;jq++){
	u=modes[iq*b+jq*c+cid];
	valx += u*legendre(xgll[gll],iq,1)*legendre(yquad[gl],jq,1);
	valy += u*legendre(ygll[gll],jq,1)*legendre(xquad[gl],iq,1);
      }
    }
    nodesX[id]=valx;
    nodesY[id]=valy;
  }
}

__global__ void compute_LLF(double* u, double* w, double* f, double* FG, 
			   double gamma, int m, int ny, int nx, int nvar, int dim, int bc, int size){
  
  int id, cid, var, cell, face, quad, mc, pc, pid, mid, fsize;
  double speed_m, speed_p, cmax;
  id = blockDim.x * blockIdx.x + threadIdx.x; 
  int a = nx+1-dim;
  int b = (ny+dim)*a;
  int c = m*b;
  int d;
  quad = id/b;
  if(dim == 0){
    face = id-quad*b;
    cell = face/a;
    face -= cell*a;
    cid = cell*a + quad*b;
    fsize = nx;
    a = 1;
  }
  else if(dim == 1){
    cell = id-quad*b;
    face = cell/a;
    cell -= face*a;
    cid = cell + quad*b;
    fsize = ny;
    a = nx;
  }
  b=m*nx*ny*(2*dim);
  c=m*nx*ny*(2*dim+1);
  d=4*m*nx*ny;
  if(id < size){
    mc = BC(face-1,fsize,bc);
    pc = BC(face,fsize,bc);
    mid = cid+pc*a+b;
    pid = cid+mc*a+c;
    speed_p = fabs(w[pid+d*(dim+1)]+sqrt(gamma*max(w[pid+d*3],P0)/max(w[pid],RHO0)));
    speed_m = fabs(w[mid+d*(dim+1)]+sqrt(gamma*max(w[mid+d*3],P0)/max(w[mid],RHO0)));
    cmax=max(speed_m,speed_p);
    for(var = 0; var < nvar; var++)
      FG[id+size*var]=0.5*(f[pid+d*var]+f[mid+d*var])+0.5*cmax*(u[pid+d*var]-u[mid+d*var]);
    
  }
}

__global__ void compute_HLLC(double* u,double* w,double* FG,double gamma,int m,int ny,int nx,int nvar,int dim,int bc,int size){
  
  int id, cid, cell, quad, mc, pc, pid, mid, face, fsize;
  int dim1, dim2;
  double cmax, cp, cm, vp, vm, sp, sm, dp, dm, pp, pm, vstar, e;
  double wgdnv[4];
  id = blockDim.x * blockIdx.x + threadIdx.x; 
  int a = nx+1-dim;
  int b = (ny+dim)*a;
  int c = m*b;
  int d;
  quad = id/b;
  if(dim == 0){
    face = id-quad*b;
    cell = face/a;
    face -= cell*a;
    cid = cell*nx + quad*nx*ny;
    fsize = nx;
    a = 1;
  }
  else if(dim == 1){
    cell = id-quad*b;
    face = cell/a;
    cell -= face*a;
    cid = cell + quad*nx*ny;
    fsize = ny;
    a = nx;
  }
  b=m*nx*ny*(2*dim); //Index for left/bottom face
  c=m*nx*ny*(2*dim+1); //Index for right/top face
  d=4*m*nx*ny;
  if(id < size){
    dim1 = dim+1;
    dim2 = 2-dim;
    mc = BC(face-1,fsize,bc);
    pc = BC(face,fsize,bc);
    pid = cid+pc*a+b; //UR=UL(face)
    mid = cid+mc*a+c; //UL=UR(face-1)
    cp = sqrt(gamma*max(w[pid+d*3],P0)/max(w[pid],RHO0));
    cm = sqrt(gamma*max(w[mid+d*3],P0)/max(w[mid],RHO0));
    cmax=max(cm,cp);
    vp = w[pid+d*dim1];
    vm = w[mid+d*dim1];
    pp = w[pid+d*3];
    pm = w[mid+d*3];
    //Compute HLL wave speed
    sm=min(vp,vm)-cmax;
    sp=max(vp,vm)+cmax;
    //Compute Lagrangian sound speed
    dm=w[mid]*(vm-sm);
    dp=w[pid]*(sp-vp);
    //Compute acoustic star state
    vstar=(dp*vp+dm*vm+(pm-pp))/(dm+dp);
    if(sm>0.0){
      wgdnv[0]=w[mid];
      wgdnv[dim1]=vm;
      wgdnv[dim2]=w[mid+d*dim2];
      wgdnv[3]=pm;
      e=u[mid+d*3];
    }
    else if(vstar>0.0){
      wgdnv[0]=w[mid]*(sm-vm)/(sm-vstar);;
      wgdnv[dim1]=vstar;
      wgdnv[dim2]=w[mid+d*dim2];
      wgdnv[3]=w[mid+d*3]+w[mid]*(sm-vm)*(vstar-vm);
      e=((sm-vm)*u[mid+d*3]-pm*vm+wgdnv[3]*vstar)/(sm-vstar);
    }
    else if(sp>0.0){
      wgdnv[0]=w[pid]*(sp-vp)/(sp-vstar);
      wgdnv[dim1]=vstar;
      wgdnv[dim2]=w[pid+d*dim2];
      wgdnv[3]=w[pid+d*3]+w[pid]*(sp-vp)*(vstar-vp);
      e=((sp-vp)*u[pid+d*3]-pp*vp+wgdnv[3]*vstar)/(sp-vstar);
    }
    else{
      wgdnv[0]=w[pid];
      wgdnv[dim1]=vp;
      wgdnv[dim2]=w[pid+d*dim2];
      wgdnv[3]=pp;
      e=u[pid+d*3];
    }
    FG[id]=wgdnv[0]*wgdnv[dim1];
    FG[id+size*dim1]=wgdnv[0]*wgdnv[dim1]*wgdnv[dim1]+wgdnv[3];
    FG[id+size*dim2]=wgdnv[0]*wgdnv[dim1]*wgdnv[dim2];
    FG[id+size*3]=wgdnv[dim1]*(e+wgdnv[3]);
  }
}


__global__ void flux_line_integral(double* edge,double* F,double* G,int m,int ny,int nx,int nvar){
  int id, ic, jc, im, jm, va;
  int q, idF, idG;
  int a = nx;
  int b = ny*a;
  int c = m*b;
  int d = m*c;
  int size = nvar*d;
  double valx,valy;
  double chsi_m = -1, chsi_p = 1;
  valx=valy=0.0;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  va  = id/d;
  ic  = id - va*d;
  jm  = ic/c;
  ic -= jm*c;
  im  = ic/b;
  ic -= im*b;
  jc  = ic/a;
  ic -= jc*a;
  idF  = ic + jc*(nx+1) + va*m*ny*(nx+1);
  idG =  ic + jc*nx     + va*m*nx*(ny+1);
  b = (nx+1)*ny;
  c = (ny+1)*nx;
  if( id < size){
    for(q = 0; q < m; q++){
      valx += (F[q*b+idF+ 1]*legendre(chsi_p,im,1)-F[q*b+idF]*legendre(chsi_m,im,1))*legendre(yquad[q],jm,1)*wyquad[q];
      valy += (G[q*c+idG+nx]*legendre(chsi_p,jm,1)-G[q*c+idG]*legendre(chsi_m,jm,1))*legendre(xquad[q],im,1)*wxquad[q];
    }
    edge[id] = valx;
    edge[id+size] = valy;
  }
}



__global__ void grad_phi_planet (double* grad, double* x, double* y, double x0, double y0, double cutoff,  double eps, int size, double t){
  int id;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  double dx, dy, r, f;
  if( id < size ){
    dx = x[id]-x0;
    dy = y[id]-y0;
    r = sqrt(dx*dx+dy*dy);
    f = r*pow(r*r+eps*eps,-1.5);
    grad[id] = -dx/r*f;
    grad[id+size] = -dy/r*f;
    dx = x[id]-0.5*cos(1.3975*t);
    dy = y[id]-0.5*sin(1.3975*t);
    r = sqrt(dx*dx+dy*dy);
    f = 1e-3*r*pow(r*r+4e-4,-1.5);
    //grad[id] += -dx/r*f;
    //grad[id+size] += -dy/r*f;
  }
}

__global__ void grad_phi (double* grad, double* x, double* y, double x0, double y0, double cutoff,  double eps, int size){
  int id;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  double dx, dy, r;
  if( id < size ){
    dx = x[id]-x0;
    dy = y[id]-y0;
    r = sqrt(dx*dx+dy*dy);
    if (r > cutoff){
      grad[id] = -dx/(r*r*r);
      grad[id+size] = -dy/(r*r*r);
    }
    else{
      grad[id] = -dx/(r*(r*r+eps*eps));
      grad[id+size] = -dy/(r*(r*r+eps*eps));
    }
  }
}
#ifdef LASRC
__global__ void get_lasource(double* w, double* s, double* grad, int size){
  int id; 
  id = blockDim.x * blockIdx.x + threadIdx.x;
  if( id < size )
       s[id] = w[id]*LASRC;
}
#endif

 
__global__ void get_source(double* w, double* s, double* grad, int size){
  int id; 
  id = blockDim.x * blockIdx.x + threadIdx.x;
  double rho,gradx,grady;
  if( id < size ){
    rho = w[id];
    gradx = grad[id];
    grady = grad[id+size];
    s[id] = 0.0;
    s[id+size] = rho*gradx;
    s[id+size*2] = rho*grady;
    s[id+size*3] = rho*(w[id+size]*gradx+w[id+size*2]*grady);
  }
}

__global__ void source_vol (double* s_vol, double* s, int m, int ny, int nx, int nvar) {
  int id, ic, jc, im, jm, va;
  int iq, jq, cid;
  int a = nx;
  int b = ny*a;
  int c = m*b;
  int d = m*c;
  int size = nvar*d;
  double val=0.0;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  va  = id/d;
  ic  = id - va*d;
  jm  = ic/c;
  ic -= jm*c;
  im  = ic/b;
  ic -= im*b;
  jc  = ic/a;
  ic -= jc*a;
  cid = ic + jc*a + va*d;
  if( id < size ){
    for( iq=0; iq<m; iq++)
      for( jq=0; jq<m; jq++)
	val += s[iq*b+jq*c+cid]*legendre(xquad[iq],im,1)*wxquad[iq]*legendre(yquad[jq],jm,1)*wyquad[jq];
    s_vol[id] = val;
  }
}

__global__ void wave_killing_bc (double* nodes, double* x, double* y, double boxlen_x, double dt, int size) {
  int id; 
  id = blockDim.x * blockIdx.x + threadIdx.x;
  double lambdadt,rho0,xn,yn,r;
  if( id < size ){
    xn = x[id];
    yn = y[id];
    r = sqrt(xn*xn+yn*yn); 
    rho0= 1./(1.+pow(r/(0.3*boxlen_x),10));
    //rho0=1.0;
    lambdadt = 0.005*dt*( r < 0.4*boxlen_x ?  0.0 : pow(1.-exp(-(r-0.4*boxlen_x)/(0.04*boxlen_x)),6.0));
    nodes[id] = (nodes[id]+lambdadt*rho0)/(1+lambdadt);
    //    nodes[id] = lambdadt;
  }
}

__global__ void HIO_limiter(double* modes, double* limited, int m, int ny, int nx, int nvar){
  int id,ic,jc,va,cid,lid,rid,bid,tid,i,j,done=0;
  int a = nx;
  int b = ny*a;
  int c = m*b;
  int d = m*c;
  int size = nx*ny*nvar;
  double val1,val2,mode1,mode2;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  va  = id/b;
  ic  = id - va*b;
  jc  = ic/a;
  ic -= jc*a;
  cid =  ic + jc*a + va*d;
  if(id < size){
    lid = BC(ic-1,nx,1) + jc*a + va*d;
    rid = BC(ic+1,nx,1) + jc*a + va*d;
    tid = ic + BC(jc+1,ny,1)*a + va*d;
    bid = ic + BC(jc-1,ny,1)*a + va*d;
    for(i=m-1;i>0;i--){
      val1 = limiting(modes,cid,lid,rid,bid,tid,i,i,m,b);
      limited[(i+i*m)*b+cid] = val1;
      mode1 = modes[(i+i*m)*b+cid];
      if (fabs(val1 - mode1) < PRC*fabs(mode1))
	break;
      //limited[(i+i*m)*b+cid] = val1;
      for(j=i-1;j>=0;j--){
	val1 = limiting(modes,cid,lid,rid,bid,tid,i,j,m,b);
	val2 = limiting(modes,cid,lid,rid,bid,tid,j,i,m,b);
	limited[(i+j*m)*b+cid] = val1;
	limited[(j+i*m)*b+cid] = val2;
	mode1 = modes[(i+j*m)*b+cid];
	mode2 = modes[(j+i*m)*b+cid];
	if (fabs(val1 - mode1) < PRC*fabs(mode1) && fabs(val2 - mode2) < PRC*fabs(mode2)){ 
	  done = 1;
	  break;
	}
	//limited[(i+j*m)*b+cid] = val1;
	//limited[(j+i*m)*b+cid] = val2;
      }
      if(done == 1)
	break;
    }
  }
}

__global__ void limit_rho(double* modes, double* uX, double* uY, double* pmodes, double eps, int k, int m, int ny, int nx, int nvar){
  int id, iq, jq, gll, gl;
  int a = nx;
  int b = ny*a;
  int size = nx*ny;
  double theta,rho_av,rho_min,valx,valy;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  if( id < size ){
    rho_av = modes[id];
    rho_min = rho_av;
   for(gll=0;gll<k;gll++){
      for(gl=0;gl<m;gl++){
	valx=uX[(gl+gll*m)*b+id];
	valy=uY[(gl+gll*m)*b+id];
	if(valx < rho_min)
	  rho_min = valx;
	if(valy < rho_min)
	  rho_min = valy;
      }
    }
    theta = min(fabs((rho_av-eps)/(rho_av-rho_min)),1.);
    if(theta<1.)
      for(iq=0;iq<m;iq++)
	for(jq=0;jq<m;jq++)
	  if (iq+jq>0)
	    pmodes[(iq+jq*m)*b+id] = theta*modes[(iq+jq*m)*b+id];
  }
}

__global__ void limit_by_pressure(double* uX, double* uY, double* pmodes,  double* modes, double gamma, double eps, int k, int m, int ny, int nx, int nvar){
  int id, va, gl, gll;
  int im, jm, qid, xs;
  int a = nx;
  int b = ny*a;
  int c = m*b;
  int d = m*c;
  int size = nx*ny;
  double tau, tau_min=1.0, P, rho, Mx, My, e, rhoav, mxav, myav, eav;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  xs = m*k*ny*nx;
  if( id < size ){
    rhoav = pmodes[id];
    mxav  = pmodes[id+d];
    myav  = pmodes[id+d*2];
    eav   = pmodes[id+d*3];
   
    for(gll=0;gll<k;gll++){
      for(gl=0;gl<m;gl++){
	qid = (gl+gll*m)*b+id;
	rho = uX[qid];
	Mx  = uX[qid+xs];
	My  = uX[qid+xs*2];
	e   = uX[qid+xs*3];
	P = (gamma-1.)*(e-0.5*(Mx*Mx+My*My)/rho);
	if(P >= eps)
	  tau = 1.;
	else
	  tau = solve_for_t(rho,Mx,My,e,rhoav,mxav,myav,eav,gamma,eps);
	if(tau < tau_min)
	  tau_min = tau;
	rho = uY[qid];
	Mx  = uY[qid+xs];
	My  = uY[qid+xs*2];
	e   = uY[qid+xs*3];
	P = (gamma-1.)*(e-0.5*(Mx*Mx+My*My)/rho);
	if(P >= eps)
	  tau = 1.;
	else
	  tau = solve_for_t(rho,Mx,My,e,rhoav,mxav,myav,eav,gamma,eps);
	if(tau < tau_min)
	  tau_min = tau;
      }
    }
    for(va = 0; va < nvar; va++)
      for( im=0; im < m; im++)
	for( jm=0; jm < m; jm++)
	  if (im+jm>0)
	    modes[(im+jm*m)*b+id+va*d] = tau_min*pmodes[(im+jm*m)*b+id+va*d];
  }
}
__global__ void check_positivity(double* uX, double* uY, double* modes, double gamma, double eps, int k, int m, int ny, int nx, int nvar){
  int id, va, gl, gll;
  int im, jm, qid, xs, negative;
  int a = nx;
  int b = ny*a;
  int c = m*b;
  int d = m*c;
  int size = nx*ny;
  double P, rho, vx, vy;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  xs = m*k*ny*nx;
  if( id < size ){
    for(gll=0;gll<k;gll++){
      for(gl=0;gl<m;gl++){
	qid = (gl+gll*m)*b+id;
	rho = uX[qid];
	vx  = uX[qid+xs]/rho;
	vy  = uX[qid+xs*2]/rho;
	P = (gamma-1.)*(uX[qid+xs*3]-0.5*(vx*vx+vy*vy)*rho);
	if( P < eps || rho < eps ){
	   negative = 1;
	   break;
	}
	rho = uY[qid];
	vx  = uY[qid+xs]/rho;
	vy  = uY[qid+xs*2]/rho;
	P = (gamma-1.)*(uY[qid+xs*3]-0.5*(vx*vx+vy*vy)*rho);
	if( P < eps || rho < eps ){
	   negative = 1;
	   break;
	}
      }
    }
    if( negative == 1)
      for(va = 0; va < nvar; va++)
	for( im=0; im < m; im++)
	  for( jm=0; jm < m; jm++)
	    if (im+jm>0)
	      modes[(im+jm*m)*b+id+va*d] = 0.0;
  }
}

__global__ void compute_dudt(double* dudt, double* flux_vol, double* edges, double* src_vol, double invdx, double invdy, int size){
  int id;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  if( id < size ){
#if defined(SRC) || defined(LASRC)
    dudt[id] = 0.5*(flux_vol[id] - invdx*edges[id] - invdy*edges[id+size] + src_vol[id]*0.5);
#else
    dudt[id] = 0.5*(flux_vol[id] - invdx*edges[id] - invdy*edges[id+size]);
#endif
  }
 
}

__global__ void sum3 (double* out, double* A, double* B, double* C, double alpha, double beta, double gamma, int size)
{  
  int id;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  if( id < size )
    out[id] = A[id]*alpha + B[id]*beta + C[id]*gamma;
}

__global__ void plus_equal (double* out, double* A, double* B, double alpha, double beta, int size)
{  
  int id;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  if( id < size )
    out[id] += A[id]*alpha + B[id]*beta;
}
__global__ void sum2 (double* out, double* A, double* B, double beta, int size)
{  
  int id;
  id = blockDim.x * blockIdx.x + threadIdx.x;
  if( id < size )
    out[id] = A[id] + B[id]*beta;
}

extern "C" void device_get_modes_from_nodes_(double** nodes, double** modes){
  int size = nx*ny*m*m*nvar;
  get_modes_from_nodes<<<(size+BLOCK-1)/BLOCK,BLOCK>>>(*nodes,*modes,m,ny,nx,nvar);
}


extern "C" void device_get_nodes_from_modes_(double** modes, double** nodes){
  int size = nx*ny*m*m*nvar;
  get_nodes_from_modes<<<(size+BLOCK-1)/BLOCK,BLOCK>>>(*modes,*nodes,m,ny,nx,nvar);
}

extern "C" void device_compute_min_dt_ (double* Dt){
  int size = nx*ny;
  double dt;
  compute_primitive<<<(size+BLOCK-1)/BLOCK,BLOCK>>>( du, w, gmma, usize, size);
  compute_min_dt<<<1,BLOCK>>>( w, pivot, gmma, cfl, dx, dy, m, usize, size);
  cudaMemcpy(&dt,pivot,sizeof(double),cudaMemcpyDeviceToHost);
  printf("dt %.14g",dt);
#ifdef SRC
  printf(" dt_grav %.14g",dt_grav);
  if (dt_grav < dt)
    dt = dt_grav;
#endif
  printf("\n");
  *Dt = dt; 
} 
extern "C" void device_compute_dt_grav_ (){
#ifdef SRC
  double delta_r = 0.1;
  double cutoff = 0.5-0.5*delta_r;
  double eps = 0.25;
#ifdef PLANET
  grad_phi_planet<<<(usize+BLOCK-1)/BLOCK,BLOCK>>>(grad,x,y,0.0,0.0,cutoff,eps,usize,0.0);
#else
  grad_phi<<<(usize+BLOCK-1)/BLOCK,BLOCK>>>(grad,x,y,0.5*boxlen_x,0.5*boxlen_y,cutoff,eps,usize);
#endif
  compute_dt_grav<<<1,BLOCK>>>( w, pivot, grad, gmma, usize);
  cudaMemcpy(&dt_grav,pivot,sizeof(double),cudaMemcpyDeviceToHost);
#endif
}
extern "C" void device_compute_limiter_(double** modes){
#ifdef LIMIT
  int size=nx*ny*nvar;
#ifdef HIO
  compute_primitive<<<(nx*ny+BLOCK-1)/BLOCK,BLOCK>>>(*modes,w,gmma,usize,nx*ny);
  size = nx*ny*(m*m-1);
  cons_to_prim<<<(size+BLOCK-1)/BLOCK,BLOCK>>>(*modes,w,gmma,m,ny,nx,usize,size);
  cudaMemcpy(pivot,w,tsize*sizeof(double),cudaMemcpyDeviceToDevice);
  HIO_limiter<<<(nx*ny*nvar+BLOCK-1)/BLOCK,BLOCK>>>(pivot,w,m,ny,nx,nvar);
  prim_to_cons<<<(size+BLOCK-1)/BLOCK,BLOCK>>>(w,*modes,gmma,m,ny,nx,usize,size);
#endif
#ifdef CP
  double eps = 1E-10;
  compute_GxGLL<<<(nx*ny*m*k+BLOCK-1)/BLOCK,BLOCK>>>(uX,uY,*modes,k,m,ny,nx,nvar);
  check_positivity<<<(nx*ny+BLOCK-1)/BLOCK,BLOCK>>>(uX,uY,*modes,gmma,eps,k,m,ny,nx,nvar);
#endif
#ifdef PP
  compute_GxGLL<<<(nx*ny*m*k+BLOCK-1)/BLOCK,BLOCK>>>(uX,uY,*modes,k,m,ny,nx,1);//Only for the density
  cudaMemcpy(pivot,*modes,tsize*sizeof(double),cudaMemcpyDeviceToDevice);
  limit_rho<<<(nx*ny+BLOCK-1)/BLOCK,BLOCK>>>(*modes,uX,uY,pivot,RHO0,k,m,ny,nx,nvar);
  compute_GxGLL<<<(nx*ny*m*k*nvar+BLOCK-1)/BLOCK,BLOCK>>>(uX,uY,pivot,k,m,ny,nx,nvar);//Now for all the quantities with the positive modes of the density
  limit_by_pressure<<<(nx*ny+BLOCK-1)/BLOCK,BLOCK>>>(uX,uY,pivot,*modes,gmma,P0,k,m,ny,nx,nvar);
#endif
#endif
}

extern "C" void device_compute_update_(int* Iter, int* SSP, double* DT, double* T){
  double dt = *DT;
  double t = *T;
  int iter = *Iter;
  int RK = *SSP;
  double* modes;
#if defined(SRC) && defined(PLANET)
  double delta_r = 0.1;
  double cutoff = 0.5-0.5*delta_r;
  double eps = 0.1;
#endif
  switch (iter){
  case 0:
    modes = du;
    break;
  case 1:
    modes = w1;
    break;
  case 2:
    modes = w2;
    break;
  case 3:
    modes = w3;
    break;
  case 4:
    modes = w4;
    break;
  }
  device_compute_limiter_(&modes);
  get_nodes_from_modes<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(modes,u_d_q,m,ny,nx,nvar);
  compute_primitive<<<(usize+BLOCK-1)/BLOCK,BLOCK>>>( u_d_q, w, gmma, usize, usize);
  compute_flux<<<(usize+BLOCK-1)/BLOCK,BLOCK>>>(u_d_q, w, flux_q1, flux_q2, usize);
  flux_vol<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(flux_v,flux_q1,flux_q2,invdx,invdy,m,ny,nx,nvar);
  compute_faces<<<(nx*ny*nvar+BLOCK-1)/BLOCK,BLOCK>>>(ufaces,modes,m,ny,nx,nvar);
  compute_primitive<<<(4*nx*ny*m+BLOCK-1)/BLOCK,BLOCK>>>(ufaces,wfaces,gmma,4*nx*ny*m,4*nx*ny*m);
#ifndef HLLC 
  compute_flux<<<(4*nx*ny*m+BLOCK-1)/BLOCK,BLOCK>>>(ufaces,wfaces,flux_f1,flux_f2,4*nx*ny*m);
  compute_LLF<<<(m*ny*(nx+1)+BLOCK-1)/BLOCK,BLOCK>>>(ufaces,wfaces,flux_f1,F,gmma,m,ny,nx,nvar,0,1,m*ny*(nx+1));
  compute_LLF<<<(m*(ny+1)*nx+BLOCK-1)/BLOCK,BLOCK>>>(ufaces,wfaces,flux_f2,G,gmma,m,ny,nx,nvar,1,1,m*(ny+1)*nx);
#else
  compute_HLLC<<<(m*ny*(nx+1)+BLOCK-1)/BLOCK,BLOCK>>>(ufaces,wfaces,F,gmma,m,ny,nx,nvar,0,1,m*ny*(nx+1));
  compute_HLLC<<<(m*(ny+1)*nx+BLOCK-1)/BLOCK,BLOCK>>>(ufaces,wfaces,G,gmma,m,ny,nx,nvar,1,1,m*(ny+1)*nx);
#endif
  flux_line_integral<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(edge,F,G,m,ny,nx,nvar);
#ifdef SRC
#ifdef PLANET
  grad_phi_planet<<<(usize+BLOCK-1)/BLOCK,BLOCK>>>(grad,x,y,0.0,0.0,cutoff,eps,usize,t);
#endif
  get_source<<<(usize+BLOCK-1)/BLOCK,BLOCK>>>(w,src,grad,usize);
  source_vol<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(src_vol,src,m,ny,nx,nvar);
#endif 
#ifdef LASRC
  get_lasource<<<(nx*ny*m*m+BLOCK-1)/BLOCK,BLOCK>>>(w,src,grad,nx*ny*m*m);
  source_vol<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(src_vol,src,m,ny,nx,nvar);
#endif
  compute_dudt<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(dudt,flux_v,edge,src_vol,invdx,invdy,nx*ny*m*m*nvar);
  if (RK==4){
    switch (iter){
    case 0:
      sum2<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(w1,du,dudt,(double)0.391752226571890*dt, tsize);
      break;
    case 1:
      sum3<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(w2,du,w1,dudt,0.444370493651235,0.555629506348765,0.368410593050371*dt, tsize);
      break;
    case 2:
      sum3<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(w3,du,w2,dudt,0.620101851488403,0.379898148511597,0.251891774271694*dt, tsize);
      break;
    case 3:
      sum3<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(w4,du,w3,dudt,0.178079954393132,0.821920045606868,0.544974750228521*dt, tsize);
      sum3<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(du,w2,w3,dudt,0.517231671970585,0.096059710526147,0.063692468666290*dt, tsize);
      break;
    case 4:
      plus_equal<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(du,w4,dudt,0.386708617503269,0.226007483236906*dt, tsize);
      cudaDeviceSynchronize();
      break;
    }
  }
  else if(RK==3){
    switch (iter){
    case 0:
      sum2<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(w1,du,dudt,dt,tsize);
      break;
    case 1:
      sum3<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(w2,du,w1,dudt,0.75,0.25,0.25*dt, tsize);
      break;
    case 2:
      sum3<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(du,du,w2,dudt,1.0/3.0,2.0/3.0,2.0/3.0*dt, tsize);
      cudaDeviceSynchronize();
      break;
     }
  }
  else if(RK==2){
    switch (iter){
    case 0:
      sum2<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(w1,du,dudt,dt,tsize);
      break;
    case 1:
      sum3<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(du,du,w1,dudt,0.5,0.5,0.5*dt, tsize);
      cudaDeviceSynchronize();
      break;
    }
  }
  else if(RK==1){
    switch (iter){
    case 0:
      sum2<<<(tsize+BLOCK-1)/BLOCK,BLOCK>>>(du,du,dudt,dt,tsize);
      cudaDeviceSynchronize();
      break;
    }
  }
}

extern "C" void gpu_allocation_ (int *Nvar, int* Nx, int* Ny, int* M, int* K, double *Bl_x, double *Bl_y, double *CFL, double *Eta, double *Gamma) {
  nvar = *Nvar;
  nx = *Nx;
  ny = *Ny;
  m = *M;
  k = *K;
  usize = m*m*nx*ny;
  tsize = usize*nvar;
  boxlen_x = *Bl_x;
  boxlen_y = *Bl_y;
  dx = boxlen_x/double(nx);
  dy = boxlen_y/double(ny);
  invdx = 1/dx;
  invdy = 1/dy;
  cfl = *CFL;
  gmma = *Gamma;
  eta = *Eta;
  cudaError_t error = cudaGetLastError();
  cudaMalloc ( &u, tsize * sizeof(double));
  cudaMalloc ( &u_eq, tsize * sizeof(double));
  cudaMalloc ( &u_d_q, tsize * sizeof(double));
  cudaMalloc ( &du, tsize * sizeof(double));
  cudaMalloc ( &w, tsize * sizeof(double));
  cudaMalloc ( &w1, tsize * sizeof(double));
  cudaMalloc ( &w2, tsize * sizeof(double));
  cudaMalloc ( &w3, tsize * sizeof(double));
  cudaMalloc ( &w4, tsize * sizeof(double));
  cudaMalloc ( &dudt, tsize * sizeof(double));
  cudaMalloc ( &ufaces, 4*nvar*nx*ny*m*sizeof(double));
  cudaMalloc ( &wfaces, 4*nvar*nx*ny*m*sizeof(double));
  cudaMalloc ( &flux_f1, 4*nvar*nx*ny*m*sizeof(double));
  cudaMalloc ( &flux_f2, 4*nvar*nx*ny*m*sizeof(double));
  cudaMalloc ( &flux_q1, tsize * sizeof(double)); 
  cudaMalloc ( &flux_q2, tsize * sizeof(double)); 
  cudaMalloc ( &flux_v,  tsize * sizeof(double)); 
  cudaMalloc ( &F, nvar*(nx+1)*ny*m * sizeof(double)); 
  cudaMalloc ( &G, nvar*nx*(ny+1)*m * sizeof(double)); 
  cudaMalloc ( &edge, tsize*2 * sizeof(double));
#if defined(SRC) || defined(LASRC)
  cudaMalloc ( &src, tsize * sizeof(double));
  cudaMalloc ( &src_vol, tsize * sizeof(double));
  cudaMalloc ( &grad, usize * 2 * sizeof(double));
#endif
  cudaMalloc ( &x, nx*ny*m*m * sizeof(double));
  cudaMalloc ( &y, nx*ny*m*m * sizeof(double));
  cudaMalloc ( &uX, nvar*ny*nx*k*m*sizeof(double));
  cudaMalloc ( &uY, nvar*ny*nx*k*m*sizeof(double));
  cudaMalloc ( &pivot, tsize * sizeof(double));
  cudaDeviceSynchronize();
  error = cudaGetLastError();
  if(error != cudaSuccess){
    printf("CUDA error gpu init: %s\n", cudaGetErrorString(error));
    exit(-1);
  }
  printf("GPU allocation done\n");
}

extern "C" void gpu_set_pointers_ (double** u_d, double** du_d, double** dudt_d, double** w_d, double** u_eq_d,
			       double** x_d, double** y_d ,double* x_quad, double* y_quad, double* w_x_quad,
				   double* w_y_quad, double* x_gll, double* y_gll, double* w_x_gll,
				   double* w_y_gll, double* Sqrt_mod) {
  cudaError_t error = cudaGetLastError(); 
  *u_d = u;
  *du_d =du;
  *dudt_d =dudt;
  *w_d = w;
  *u_eq_d = u_eq;
  *x_d = x;
  *y_d = y;
  cudaMemcpyToSymbol(sqrt_mod,Sqrt_mod,sizeof(double)*m);
  cudaMemcpyToSymbol(xquad,x_quad,sizeof(double)*m);
  cudaMemcpyToSymbol(yquad,y_quad,sizeof(double)*m);
  cudaMemcpyToSymbol(wxquad,w_x_quad,sizeof(double)*m);
  cudaMemcpyToSymbol(wyquad,w_y_quad,sizeof(double)*m);
  cudaMemcpyToSymbol(xgll,x_gll,sizeof(double)*k);
  cudaMemcpyToSymbol(ygll,y_gll,sizeof(double)*k);
  cudaMemcpyToSymbol(wxgll,w_x_gll,sizeof(double)*k);
  cudaMemcpyToSymbol(wygll,w_y_gll,sizeof(double)*k);
#ifdef LASRC
  cudaMemset(grad,10,2*usize*sizeof(double));
  cudaMemset(src,0,tsize*sizeof(double));
#endif
  cudaDeviceSynchronize();
  error = cudaGetLastError();
  if(error != cudaSuccess){
    printf("CUDA error gpu init: %s\n", cudaGetErrorString(error));
    exit(-1);
  }
  printf("FORTRAN-CUDA pointers done\n");
}


extern "C" void h2d_ (double *array, double **darray, int* Size) {
  int size = *Size;
  cudaError_t error = cudaGetLastError();
  cudaDeviceSynchronize();
  cudaMemcpy( *darray, array,  size * sizeof(double) ,cudaMemcpyHostToDevice);
  error = cudaGetLastError();
  if(error != cudaSuccess){
    printf("CUDA error h2d: %s\n", cudaGetErrorString(error));
    exit(-1);
  }
}

extern "C" void d2h_ (double **darray, double *array, int* Size) {
  int size = *Size;
  cudaError_t error = cudaGetLastError();
  cudaDeviceSynchronize();
  cudaMemcpy( array, *darray,  size * sizeof(double) ,cudaMemcpyDeviceToHost);
  error = cudaGetLastError();
  if(error != cudaSuccess){
    printf("CUDA error d2h: %s\n", cudaGetErrorString(error));
    exit(-1);
  }
}

extern "C" void setdevice_ (int *Device) {
  int device = *Device;
  cudaSetDevice(device);
  cudaDeviceProp prop;
  cudaGetDeviceProperties(&prop, device);
  printf("Device Number: %d\n", device);
  printf("Device name: %s\n", prop.name);
  printf("Device Memory: %d\n",prop.totalGlobalMem);
}

extern "C" void devices_ () {
  int nDevices;
  cudaGetDeviceCount(&nDevices);
  printf("Devices: %d\n",nDevices);
  for (int i = 0; i < nDevices; i++) {
    cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, i);
    printf("Device Number: %d\n", i);
    printf("  Name: %s\n", prop.name);
    printf("  Compute mode: %d\n", prop.computeMode);
    printf("  Memory Capacity (bytes): %d\n",
           prop.totalGlobalMem/8);
    printf("  Multiprocessors: %d\n\n", prop.multiProcessorCount); 
  }
}
