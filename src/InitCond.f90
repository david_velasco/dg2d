!---------
subroutine get_coords()
  use parameters_dg_2d
  implicit none
  
  !internal vars
  real(kind=8)::dx, dy
  integer::i,j, nj, ni

  ! get quadrature
  call gl_quadrature(x_quad,w_x_quad,m)
  call gl_quadrature(y_quad,w_y_quad,m)
  call gll_quadrature(x_gll,w_x_gll,k)
  call gll_quadrature(y_gll,w_y_gll,k)
  
  do i=1,m
    sqrt_mod(i)=sqrt((2.0*dble(i-1)+1.0))
  end do  
  dx = boxlen_x/dble(nx)
  dy = boxlen_y/dble(ny)
  do i=1,nx
    do j=1,ny
      do ni =1,m
        do nj = 1,m
          x(i,j,ni,nj) = (i-0.5)*dx + dx/2.0*x_quad(ni)! - 0.5*boxlen_x
          y(i,j,ni,nj) = (j-0.5)*dy + dy/2.0*y_quad(nj)! - 0.5*boxlen_y
        end do
      end do
    end do
  end do

end subroutine get_coords
!-------
subroutine get_equilibrium_solution()
  ! get equilibrium solution for primitive variables
  use parameters_dg_2d
  real(kind=8)::rho_0,p_0,g

  select case (nequilibrium)
  case(1)
    u_eq(:,:,:,:,1) = exp(-(x+y))
    u_eq(:,:,:,:,2) = 0
    u_eq(:,:,:,:,3) = 0
    u_eq(:,:,:,:,4) = exp(-(x+y))
  case(2)
    rho_0 = 1.21
    p_0 = 1
    g = 1
    u_eq(:,:,:,:,1) = rho_0*exp(-(rho_0*g/p_0)*(x+y))
    u_eq(:,:,:,:,2) = 0
    u_eq(:,:,:,:,3) = 0
    u_eq(:,:,:,:,4) = p_0*exp(-(rho_0*g/p_0)*(x+y))
  case(3)
    u_eq(:,:,:,:,:) = 0.0
  end select

end subroutine get_equilibrium_solution
!-------
subroutine get_initial_conditions(u,w)
  use parameters_dg_2d
  implicit none
  real(kind=8),dimension(1:nx,1:ny,1:m,1:m,1:nvar)::u,w  
  integer::i,j,inti,intj, jcell, icell, n
  ! internal variables
  real(kind=8)::rho_0, p_0, g
  real(kind=8)::dpi=acos(-1d0)
  real(kind=8)::beta
  real(kind=8)::r0, omega, cs, grav, Ms, eps, h
  real(kind=8)::x_dash, y_dash, r, rho_d, delta_r, x_center, y_center

  select case (ninit)
  case(1) !Linear advection of pulse
    w(:,:,:,:,1) = exp(-((x(:,:,:,:)-boxlen_x/2.)**2+(y(:,:,:,:)-boxlen_y/2.)**2)*20)
    w(:,:,:,:,2) = 1.0
    w(:,:,:,:,3) = 1.0
    w(:,:,:,:,4) = minval(w(:,:,:,:,1))
  case(2) !hydrostatic equilibrium with a pulse on pressure
    rho_0 = 1.21
    p_0 = 1.
    g = 1.
    w(:,:,:,:,1) = rho_0*exp(-(rho_0*g/p_0)*(x+y))
    w(:,:,:,:,2) = 0
    w(:,:,:,:,3) = 0
    w(:,:,:,:,4) = p_0*exp(-(rho_0*g/p_0)*(x+y)) + eta*&
    &exp(-100*(rho_0*g/p_0)*((x-0.3)**2+(y-0.3)**2))
  case(3) !2d riemann problem case 4
    do i = 1,nx
      do j = 1,ny
        do inti = 1,m
          do intj = 1,m
            if (x(i,j,inti,intj)>=0.5 .and. y(i,j,inti,intj)>=0.5) then
              w(i,j,inti,intj,1) = 1.
              w(i,j,inti,intj,2)  = 0.
              w(i,j,inti,intj,3)  = 0.
              w(i,j,inti,intj,4)  = 1.
            else if (x(i,j,inti,intj)<0.5 .and. y(i,j,inti,intj)>=0.5) then
              w(i,j,inti,intj,1) = 0.5197
              w(i,j,inti,intj,2) = -0.7259
              w(i,j,inti,intj,3) = 0.
              w(i,j,inti,intj,4) = 0.4
            else if (x(i,j,inti,intj)<0.5 .and. y(i,j,inti,intj)<0.5) then
              w(i,j,inti,intj,1) = 0.1072
              w(i,j,inti,intj,2) = -0.7259
              w(i,j,inti,intj,3) = -1.4045
              w(i,j,inti,intj,4) = 0.0439
            else if (x(i,j,inti,intj)>=0.5 .and. y(i,j,inti,intj)<0.5) then
              !else
              w(i,j,inti,intj,1) = 0.2579
              w(i,j,inti,intj,2) = 0.0
              w(i,j,inti,intj,3) = -1.4045
              w(i,j,inti,intj,4) = 0.15
            end if
          end do
        end do
      end do
    end do
  case(4) ! another riemann problem
    do i = 1,nx
      do j = 1,ny
        do inti = 1,m
          do intj = 1,m
            if (x(i,j,inti,intj)>=0.5 .and. y(i,j,inti,intj)>=0.5) then
              w(i,j,inti,intj,1) = 1.5
              w(i,j,inti,intj,2)  = 0.
              w(i,j,inti,intj,3)  = 0.
              w(i,j,inti,intj,4)  = 1.5
            else if (x(i,j,inti,intj)<0.5 .and. y(i,j,inti,intj)>=0.5) then
              w(i,j,inti,intj,1) = 0.5323
              w(i,j,inti,intj,2) = 1.206
              w(i,j,inti,intj,3) = 0.
              w(i,j,inti,intj,4) = 0.3
            else if (x(i,j,inti,intj)<0.5 .and. y(i,j,inti,intj)<0.5) then
              w(i,j,inti,intj,1) = 0.138
              w(i,j,inti,intj,2) = 1.206
              w(i,j,inti,intj,3) = 1.206
              w(i,j,inti,intj,4) = 0.029
            else if (x(i,j,inti,intj)>=0.5 .and. y(i,j,inti,intj)<0.5) then
              !else
              w(i,j,inti,intj,1) = 0.5323
              w(i,j,inti,intj,2) = 0.0
              w(i,j,inti,intj,3) = 1.206
              w(i,j,inti,intj,4) = 0.3
            end if
          end do
        end do
      end do
    end do
  case(5)
    do i = 1,nx
      do j = 1,ny
        do inti = 1,m
          do intj = 1,m
            if (x(i,j,inti,intj) + y(i,j,inti,intj) >=0.5) then
              w(i,j,inti,intj,1) = 1.
              w(i,j,inti,intj,2)  = 0.
              w(i,j,inti,intj,3)  = 0.
              w(i,j,inti,intj,4)  = 1.
            else if (x(i,j,inti,intj) + y(i,j,inti,intj) < 0.5) then
              w(i,j,inti,intj,1) = 0.125
              w(i,j,inti,intj,2) = 0.
              w(i,j,inti,intj,3) = 0.
              w(i,j,inti,intj,4) = 0.4
            end if
          end do
        end do
      end do
    end do
  case(6) ! Isentropic vortex
    ! boxlen_x = 10 boxlen_y = 10
    x_center = 0.5*boxlen_x
    y_center = 0.5*boxlen_y
    ! gamma = 7./5.
    beta = 5.
    do i = 1,nx
      do j = 1,ny
        do inti = 1,m
          do intj = 1,m
            x_dash = x(i,j,inti,intj) - x_center
            y_dash = y(i,j,inti,intj) - y_center
            r = sqrt(x_dash**2 + y_dash**2)

            w(i,j,inti,intj,1) = (1. &
            &-((gamma-1.)*beta**2)/(8*gamma*dpi**2)*exp(1-&
            &r**2))**(1/(gamma-1))

            w(i,j,inti,intj,2) = 1- &
            & (y_dash)*beta/(2*dpi)*exp((1.-r**2)/2.)

            w(i,j,inti,intj,3)  = 1+ &
            &(x_dash)*beta/(2*dpi)*exp((1.-r**2)/2.)

            w(i,j,inti,intj,4)  = w(i,j,inti,intj,1)**gamma
          end do
        end do
      end do
    end do
  case(7) ! smooth rotating disk
    ! initialize variables for smooth rotating disk
    ! boxlen has to be 6x6
    p_0 = 10e-5
    rho_0 = 10e-5
    rho_d = 1
    delta_r = 0.1
    x_center = 0.5*boxlen_x
    y_center = 0.5*boxlen_y
    w(:,:,:,:,4)  = p_0
    w(:,:,:,:,2)  = 0.0
    w(:,:,:,:,3)  = 0.0 
    do i = 1,nx
      do j = 1,ny
        do inti = 1,m
          do intj = 1,m
            x_dash = x(i,j,inti,intj) - x_center
            y_dash = y(i,j,inti,intj) - y_center
            r = sqrt(x_dash**2 + y_dash**2)

            if (r<0.5-delta_r/2.) then
              w(i,j,inti,intj,1) = rho_0
            else if ((r<0.5+delta_r/2.).and.(r >= 0.5-delta_r/2.)) then
              w(i,j,inti,intj,1) = (rho_d-rho_0)/delta_r * (r - (0.5-delta_r/2.)) + rho_0
            else if ((r >= 0.5+delta_r/2.).and.(r < 2-delta_r/2.))  then
              w(i,j,inti,intj,1) = rho_d
            else if ((r >= 2-delta_r/2.).and.(r < 2+delta_r/2.))  then
              w(i,j,inti,intj,1) = (rho_0-rho_d)/delta_r * (r - (2-delta_r/2.)) + rho_d
            else if (r >= 2+delta_r/2.) then
              w(i,j,inti,intj,1) = rho_0
            end if
            if ((r > 0.5-2.*delta_r).and.(r < 2.+2.*delta_r)) then
               w(i,j,inti,intj,2) = -y_dash/sqrt(r**3)
               w(i,j,inti,intj,3) =  x_dash/sqrt(r**3)
            endif

          end do
        end do
      end do
    end do

  case(8) ! square advection
    p_0 = 2.5
    rho_0 = 1
    rho_d = 4*rho_0
    delta_r = 0.1
    x_center = 0.5
    y_center = 0.5
    w(:,:,:,:,4) = p_0
    do i = 1,nx
      do j = 1,ny
        do inti = 1,m
          do intj = 1,m
            x_dash = x(i,j,inti,intj) - x_center
            y_dash = y(i,j,inti,intj) - y_center

            if ((abs(x_dash)<=0.25).and.(abs(y_dash)<=0.25)) then
              w(i,j,inti,intj,1) = rho_d
            else
              w(i,j,inti,intj,1) = rho_0
            end if

            w(i,j,inti,intj,2) = 100.0 !1.0
            w(i,j,inti,intj,3) = 50.0 !1.0

            w(i,j,inti,intj,4)  = p_0
          end do
        end do
      end do
    end do

  case(9) ! 1d discontinuous pulse advection in x
    p_0 = 1
    rho_0 = 1
    rho_d = rho_0*4
    x_center = 0.5
    y_center = 0.5
    w(:,:,:,:,4) = p_0
    do i = 1,nx
      do j = 1,ny
        do inti = 1,m
          do intj = 1,m
            x_dash = x(i,j,inti,intj) - x_center
            y_dash = y(i,j,inti,intj) - y_center

            if ((abs(x_dash)<=0.25)) then
              w(i,j,inti,intj,1) = rho_d
            else
              w(i,j,inti,intj,1) = rho_0
            end if

            w(i,j,inti,intj,2) = 1.0
            w(i,j,inti,intj,3) = 0.0

          end do
        end do
      end do
    end do 
  case(10) ! 1d discontinuous pulse advection in x
    p_0 = 1
    rho_0 = 1
    rho_d = rho_0*4
    x_center = 0.5
    y_center = 0.5
    w(:,:,:,:,4) = p_0
    do i = 1,nx
      do j = 1,ny
        do inti = 1,m
          do intj = 1,m
            x_dash = x(i,j,inti,intj) - x_center
            y_dash = y(i,j,inti,intj) - y_center

            if ((abs(y_dash)<=0.25)) then
              w(i,j,inti,intj,1) = rho_d
            else
              w(i,j,inti,intj,1) = rho_0
            end if

            w(i,j,inti,intj,2) = 0.0
            w(i,j,inti,intj,3) = 1.0

          end do
        end do
      end do
    end do
 case(11)
    rho_0 = 1
    r0 = 0.375*boxlen_x
    n = 10
    grav = 1
    Ms = 1
    eps = 1E-1
    h = 0.03
    do i = 1,nx
      do j = 1,ny
        do inti = 1,m
          do intj = 1,m
            r = sqrt(x(i,j,inti,intj)**2 + y(i,j,inti,intj)**2) 
            omega = sqrt(grav*Ms/sqrt(r**2+eps**2)**3);
            cs = h*omega*r 
            w(i,j,inti,intj,1) = rho_0/(1.+(r/r0)**n)
            w(i,j,inti,intj,2) = -omega*sqrt(1.+h**2)*y(i,j,inti,intj)
            w(i,j,inti,intj,3) = omega*sqrt(1.+h**2)*x(i,j,inti,intj)
            w(i,j,inti,intj,4) = cs**2*w(i,j,inti,intj,1)
          end do
        end do
      end do
    end do
 case(12)
    do i = 1,nx
      do j = 1,ny
        do inti = 1,m
          do intj = 1,m
            r = sqrt(x(i,j,inti,intj)**2 + y(i,j,inti,intj)**2) 
            omega = sqrt(grav*Ms/sqrt(r**2+eps**2)**3);
            cs = h*omega*r 
            w(i,j,inti,intj,1) = rho_0/(1.+(r/r0)**n)
            w(i,j,inti,intj,2) = -omega*sqrt(1.+h**2)*y(i,j,inti,intj)
            w(i,j,inti,intj,3) = omega*sqrt(1.+h**2)*x(i,j,inti,intj)
            w(i,j,inti,intj,4) = cs**2*w(i,j,inti,intj,1)
          end do
        end do
      end do
    end do
 end select
  call compute_conservative(w,u,nx,ny,m)
  
end subroutine get_initial_conditions
