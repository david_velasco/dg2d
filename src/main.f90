program main
  USE ISO_C_BINDING
  use parameters_dg_2d
  real(kind=8),dimension(1:nx,1:ny,1:m,1:m,1:nvar)::u,w  
  !device pointers
  type (c_ptr) :: u_d, du_d,dudt_d, w_d, u_eq_d 
  type (c_ptr) :: x_d, y_d
  !Initialization of fields
  call get_coords()
  call get_initial_conditions(u,w)
  call get_equilibrium_solution()
 
  !device allocation and memory copies 
  call devices()
  call setdevice(dev)
  call gpu_allocation(nvar,nx,ny,m,k,boxlen_x,boxlen_y,cfl,eta,gamma)
  call gpu_set_pointers(u_d,du_d,dudt_d,w_d,u_eq_d,x_d,y_d &
            & ,x_quad,y_quad,w_x_quad,w_y_quad,x_gll,y_gll,w_x_gll,w_y_gll,sqrt_mod)
  call h2d(u,u_d,nvar*nx*ny*m*m)
  call h2d(w,w_d,nvar*nx*ny*m*m)
  call h2d(u_eq,u_eq_d,nvar*nx*ny*m*m)
  call h2d(x,x_d,nx*ny*m*m)
  call h2d(y,y_d,nx*ny*m*m)
  call writedesc()
  call evolve(u, u_d, du_d, dudt_d)
  
end program main

subroutine evolve(u,u_d,du_d,dudt_d)
  ! eat real nodal values, spit out nodal values
  USE ISO_C_BINDING
  use parameters_dg_2d
  implicit none
  type (c_ptr) :: u_d, du_d, dudt_d
  ! internal variables
  real(kind=8)::t,dt,top,start,finish,time
  real(kind=8)::cmax, dx, dy, cs_max,v_xmax,v_ymax
  real(kind=8),dimension(1:nx,1:ny,1:m,1:m,1:nvar)::u,dudt,w1,w2,w3,w4,delta_u,nodes
  integer::iter, n, i, j, irk, rkt, ssp
  integer::snap_counter
  integer::var
 
  call device_compute_dt_grav();!It will only do something if the SRC is define over CUDA
  call device_get_modes_from_nodes(u_d,du_d)
  call device_compute_limiter(du_d)
  snap_counter = 0
  call writefields(snap_counter,du_d,delta_u)
  t=0
  top=dtop
  iter=0
  ssp=m
  if(ssp<4)then
     rkt=ssp-1
  else
     rkt=ssp
  end if
 
  call cpu_time(start)
  do while(t < tend)
    ! Compute time step
    call device_compute_min_dt(dt)
      
    do irk=0,rkt !RUNGE KUTTA SUBSTEPING
       call device_compute_update(irk,ssp,dt,t)
    end do
    t=t+dt
    iter=iter+1
    write(*,*)'time=',iter,t,dt
    
    if ((make_movie).and.(t>=top))then
       snap_counter = snap_counter + 1
       call writefields(snap_counter,du_d,delta_u)
       top = top + dtop
    end if
 end do
 call cpu_time(finish)
 print '("cpu average Time = ",e10.3," seconds.")',(finish-start)/dble(iter)
end subroutine evolve






