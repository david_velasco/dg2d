module parameters_dg_2d
  ! solver parameter
  integer,parameter::nx=32
  integer,parameter::ny=32
  integer,parameter::m=4
  integer,parameter::k=(m+3)/2
  integer,parameter::nvar=4
  integer,parameter::riemann=2
  logical,parameter::use_limiter=.false.
  logical,parameter::make_movie=.true.
  character(LEN=3),parameter::solver='RK4' !or EQL to use the equilibrium solution ^^
  character(LEN=3),parameter::limiter_type='ROS' !or EQL to use the equilibrium solution ^^
  ! Problem set-up
  integer,parameter::ninit=8
  integer,parameter::bc=1
  integer,parameter::nequilibrium=3
  integer,parameter::source=1

  real(kind=8)::tend=10.0
  real(kind=8)::dtop=0.1
  real(kind=8)::boxlen_x=1.0
  real(kind=8)::boxlen_y=1.0
  real(kind=8)::gamma=1.4!5./3.

  real(kind=8)::cfl = 0.2 

  real(kind=8)::eta=0.1

  ! misc commons
  real(kind=8),dimension(1:m)::x_quad, w_x_quad
  real(kind=8),dimension(1:m)::y_quad, w_y_quad
  real(kind=8),dimension(1:k)::x_gll, w_x_gll
  real(kind=8),dimension(1:k)::y_gll, w_y_gll
  real(kind=8),dimension(1:m)::sqrt_mod
  real(kind=8),dimension(1:nx,1:ny,1:m,1:m)::x,y
  real(kind=8),dimension(1:nx,1:ny,1:m,1:m,1:nvar)::u_eq
  !real(kind=8)::M=0
  integer,parameter::interval = 10000
  character(len=10)::folder='sq32DG4'
  integer,parameter::dev = 0
end module parameters_dg_2d
