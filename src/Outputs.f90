
subroutine writefields(number,device,host)
  use ISO_C_BINDING
  use parameters_dg_2d
  implicit none
  type (c_ptr) :: device
  real(kind=8),dimension(1:nx,1:ny,1:m,1:m,1:nvar)::host
  integer :: number
  character(len=10)::filename
  call d2h(device,host,nvar*nx*ny*m*m)
  write(filename,'(a, i5.5)') 'modes', number
  open(unit=8,status="REPLACE",file= trim(folder)//"/"//filename//".dat",&
       &form='UNFORMATTED',access='direct',recl=nx*ny*m*m*nvar*8)
  write(8,rec=1)host
  close(8)
end subroutine writefields

subroutine writedesc()
  use parameters_dg_2d
  implicit none
  call system('mkdir -p ' //folder )
  open(unit=10,status="REPLACE",file= trim(folder)//"/Descriptor.dat")
  write(10,*)nx,ny,m,nvar,boxlen_x,boxlen_y,gamma
  close(10)
end subroutine writedesc
