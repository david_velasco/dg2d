 
subroutine compute_update(delta_u,dudt)
  use parameters_dg_2d
  implicit none
  real(kind=8),dimension(1:m,1:m,1:ny,1:nx,1:nvar)::delta_u,dudt
  real(kind=8),dimension(1:nvar,1,1:m,1:nx+1,1:ny)::F
  real(kind=8),dimension(1:nvar,1:m,1,1:nx, 1:ny+1)::G
  real(kind=8),dimension(1:m,1:m,1:ny,1:nx,1:nvar)::s, source_vol
  !===========================================================
  ! This routine computes the DG update for the input state u.
  !===========================================================
  real(kind=8),dimension(1:nx,1:ny,1:m,1:m,1:nx+1,1:ny+1)::x_faces, y_faces
  real(kind=8),dimension(1:nvar,1:m,1:m)::u_quad,source_quad

  real(kind=8),dimension(1:nvar):: u_temp
  real(kind=8),dimension(1:nvar,2):: flux_temp
  real(kind=8),dimension(1:m,1:m,1:ny,1:nx,1:nvar)::u_delta_quad
  real(kind=8),dimension(1:m,1:m,1:ny,1:nx,1:nvar)::flux_quad1
  real(kind=8),dimension(1:m,1:m,1:ny,1:nx,1:nvar)::flux_quad2


  real(kind=8),dimension(1:m,1:m,1:ny,1:nx,1:nvar)::flux_vol1, flux_vol2
  real(kind=8),dimension(1:nvar,1:nx,1:ny,1,1:m)::u_left,u_right
  real(kind=8),dimension(1:nvar,1:nx,1:ny,1:m,1)::u_top, u_bottom
  real(kind=8),dimension(1:nvar,1:nx,1:ny,1:m,1,2)::flux_top, flux_bottom

  real(kind=8),dimension(1:nvar)::flux_riemann,u_tmp
  real(kind=8),dimension(1:nvar,1:nx+1,1:ny+1)::flux_face,flux_face_eq
  !real(kind=8),dimension(1:nx+1,1:ny+1)::x_faces
  real(kind=8),dimension(1:nvar,1:nx,1:ny,1:m,1:m,2)::flux_left,flux_right
  real(kind=8),dimension(1:m,1:m,1:ny,1:nx,1:nvar,4):: edge

  integer::icell,i,j,iface,ileft,iright,ivar, node, jface
  integer::intnode,jntnode,jcell, edge_num, one
  real(kind=8)::legendre,legendre_prime
  real(kind=8)::chsi_left=-1,chsi_right=+1
  real(kind=8)::chsi_bottom=-1,chsi_top=+1
  real(kind=8)::dx,dy
  real(kind=8)::cmax,oneoverdx,c_left,c_right,oneoverdy
  real(kind=8)::x_right,x_left
  real(kind=8),dimension(1:nvar,1:m):: u_delta_r, u_delta_l, u_delta_t, u_delta_b
  real(kind=8),dimension(1:nvar,1:nx,1:ny,1:m)::u_delta_left,u_delta_right,u_delta_top,u_delta_bottom
  real(kind=8),dimension(1:nvar,1:(nx+1))::u_face_eq, w_face_eq

  dx=boxlen_x/dble(nx)
  dy=boxlen_y/dble(ny)
  oneoverdx=1./dble(dx)
  oneoverdy=1./dble(dy)

  ! get equilibrium quantities

  !==================================
  ! Compute volume integral for Flux
  !==================================

  u_delta_quad(:,:,:,:,:) = 0.0
  flux_vol1(:,:,:,:,:) = 0.0
  flux_vol2(:,:,:,:,:) = 0.0
  !flux_quad(:,:,:,:,:,:) = 0.0

  call get_nodes_from_modes(delta_u,u_delta_quad,nx,ny,m)
  call compute_flux(u_delta_quad,flux_quad1,flux_quad2,nx,ny,m)


  do icell=1,nx
    do jcell=1,ny
      !==================================
      ! Compute flux at quadrature points
      !==================================
      !write(*,*) u_delta_quad
      !================================
      ! Compute volume integral DG term
      !================================
      ! Loop over modes
      do i = 1,m
        do j = 1,m
          flux_vol1(j,i,jcell,icell,1:nvar) = 0.0
          do intnode=1,m
            do jntnode=1,m
              flux_vol1(j,i,jcell,icell,1:nvar)=flux_vol1(j,i,jcell,icell,1:nvar)+ &
              & 0.25*flux_quad1(jntnode,intnode,jcell,icell,1:nvar)* & ! x direction
              & legendre_prime(x_quad(intnode),i-1)* &
              & w_x_quad(intnode)*&
              & legendre(y_quad(jntnode),j-1)* &
              & w_y_quad(jntnode)! * dx*dy/4.
            end do
          end do

          do intnode=1,m
            do jntnode=1,m
              flux_vol2(j,i,jcell,icell,1:nvar) = flux_vol2(j,i,jcell,icell,1:nvar)&
              & + 0.25*flux_quad2(jntnode,intnode,jcell,icell,1:nvar)* & !y direction
              & legendre_prime(y_quad(jntnode),j-1)* &
              & w_y_quad(jntnode)*&
              & legendre(x_quad(intnode),i-1)* &
              & w_x_quad(intnode)! * dx*dy/4.
            end do
          end do
        end do
      end do
    end do
  end do


  u_delta_l(:,:) = 0.0
  u_delta_r(:,:) = 0.0
  u_delta_t(:,:) = 0.0
  u_delta_b(:,:) = 0.0


  do icell=1,nx
    do jcell=1,ny
      !==============================
      ! Compute left and right states
      ! computing the value AT the node -1 and 1
      !==============================
      !u_delta(1:nvar,icell)=0.0
      ! Loop over modes
      u_delta_l = 0.
      u_delta_r = 0.
      do i=1,m
        do j=1,m
          do intnode = 1,m
            u_delta_l(1:nvar, intnode) = u_delta_l(1:nvar, intnode) + delta_u(j,i,jcell,icell,1:nvar)*&
            &legendre(chsi_left,i-1)*legendre(y_quad(intnode),j-1)
            u_delta_r(1:nvar, intnode) = u_delta_r(1:nvar, intnode) + delta_u(j,i,jcell,icell,1:nvar)*&
            &legendre(chsi_right,i-1)*legendre(y_quad(intnode),j-1)
          end do
        end do
      end do
      u_delta_left(1:nvar,icell,jcell,:) = u_delta_l(1:nvar,:) !(u_delta_r+u_delta_l)/2. - (uu_r+uu_l)/2.
      u_delta_right(1:nvar,icell,jcell,:) = u_delta_r(1:nvar,:) !(u_delta_r+u_delta_l)/2. - (uu_r+uu_l)/2.

      ! compute equilibrium on the fly
      u_left(1:nvar,icell,jcell,1,:) = u_delta_left(1:nvar,icell,jcell,:)
      u_right(1:nvar,icell,jcell,1,:) = u_delta_right(1:nvar,icell,jcell,:)
    end do
  end do

  do icell=1,nx
    do jcell=1,ny
      !==============================
      ! Compute left and right states
      ! computing the value AT the node -1 and 1
      !==============================
      !u_delta(1:nvar,icell)=0.0
      ! Loop over modes
      u_delta_b = 0.
      u_delta_t = 0.
      do i=1,m
        do j=1,m
          do intnode = 1,m
             u_delta_b(1:nvar,intnode) = u_delta_b(1:nvar, intnode) + delta_u(j,i,jcell,icell,1:nvar)*&
                  & legendre(chsi_bottom,j-1)*legendre(x_quad(intnode),i-1)

             u_delta_t(1:nvar,intnode) = u_delta_t(1:nvar, intnode) + delta_u(j,i,jcell,icell,1:nvar)*&
                  & legendre(chsi_top,j-1)*legendre(x_quad(intnode),i-1)
          end do
        end do
      end do
      u_delta_bottom(1:nvar,icell,jcell,:) = u_delta_b(1:nvar,:) !(u_delta_r+u_delta_l)/2. - (uu_r+uu_l)/2.
      u_delta_top(1:nvar,icell,jcell,:) = u_delta_t(1:nvar,:) !(u_delta_r+u_delta_l)/2. - (uu_r+uu_l)/2.

      ! compute equilibrium on the fly
      u_bottom(1:nvar,icell,jcell,:,1) = u_delta_bottom(1:nvar,icell,jcell,:)
      u_top(1:nvar,icell,jcell,:,1) = u_delta_top(1:nvar,icell,jcell,:)
    end do
  end do

  ! fluxes
  F(:,:,:,:,:) = 0.
  G(:,:,:,:,:) = 0.
  one = 1
  do icell = 1,nx
    do jcell = 1,ny
      do i = 1,m
        call compute_flux_int(u_left(1:nvar,icell,jcell,1,i),flux_left(1:nvar,icell,jcell,1,i,:))
        call compute_flux_int(u_right(1:nvar,icell,jcell,1,i),flux_right(1:nvar,icell,jcell,1,i,:))
        call compute_flux_int(u_top(1:nvar,icell,jcell,i,1),flux_top(1:nvar,icell,jcell,i,1,:))
        call compute_flux_int(u_bottom(1:nvar,icell,jcell,i,1),flux_bottom(1:nvar,icell,jcell,i,1,:))
     end do
    end do
  end do

  do j = 1, ny
    do iface = 1, nx+1
      ileft = iface-1
      iright = iface

      call get_boundary_conditions(ileft,2)
      call get_boundary_conditions(iright,2)

      ! subroutine compute_llflux(uleft,uright, f_left,f_right, fgdnv)
      do intnode = 1, m
        call compute_llflux(u_right(1:nvar,ileft,j,1,intnode),u_left(1:nvar,iright,j,1,intnode),&
        &flux_right(1:nvar,ileft,j,1,intnode,1),&
        &flux_left(1:nvar,iright,j,1,intnode,1),F(1:nvar,1,intnode,iface,j),1)
      end do

    end do
  end do

  do i = 1,nx
    do jface = 1,ny+1
      ileft = jface-1
      iright = jface

      call get_boundary_conditions(ileft,2)
      call get_boundary_conditions(iright,2)

      do intnode = 1, m
        call compute_llflux(u_top(1:nvar,i,ileft,intnode,1),u_bottom(1:nvar,i,iright,intnode,1),&
        &flux_top(1:nvar,i,ileft,intnode,1,2),&
        &flux_bottom(1:nvar,i,iright,intnode,1,2),G(1:nvar,intnode,1,i,jface),2)
      end do

    end do
  end do

  !========================
  ! Compute flux line integral
  !========================
  ! Loop over cells]
  edge(:,:,:,:,:,:)=0.0
  do icell = 1,nx
    do jcell = 1,ny
      do i = 1,m
        do j = 1, m
          do intnode = 1,m
            edge(j,i,jcell,icell,1:nvar, 1) = &
            &edge(j,i,jcell,icell,1:nvar, 1) + &
            &0.5*F(1:nvar,1, intnode, icell + 1, jcell)*legendre(chsi_right,i-1)*legendre(x_quad(intnode),j-1)*w_x_quad(intnode)!&
            !&* dx/2.

            edge(j,i,jcell,icell,1:nvar,2) = &
            &edge(j,i,jcell,icell,1:nvar,2) + &
            &0.5*F(1:nvar,1, intnode, icell, jcell)*legendre(chsi_left,i-1)*legendre(x_quad(intnode),j-1)*w_x_quad(intnode)!&
            !&* dx/2.
          end do
        end do
      end do

      do i = 1,m
        do j =1,m
          do intnode = 1,m
            !edge(j,i,jcell,icell,1:nvar,3) = &
            !&edge(j,i,jcell,icell,1:nvar, 3) + &
            !&G(1:nvar, intnode, 1, icell, jcell+1)*legendre(chsi_right,j-1)*legendre(x_quad(intnode),i-1)*w_x_quad(intnode)

            edge(j,i,jcell,icell,1:nvar,3) = &
            &edge(j,i,jcell,icell,1:nvar,3) + &
            &0.5*G(1:nvar,intnode, 1, icell, jcell+1)*legendre(chsi_right,j-1)*legendre(x_quad(intnode),i-1)*w_x_quad(intnode)!&
            !&* dx/2.

            edge(j,i,jcell,icell,1:nvar,4) = &
            &edge(j,i,jcell,icell,1:nvar,4) + &
            &0.5*G(1:nvar, intnode, 1, icell, jcell)*legendre(chsi_left,j-1)*legendre(x_quad(intnode),i-1)*w_x_quad(intnode)!&
            !&* dx/2.
          end do
        end do
      end do
    end do
  end do


  source_vol(:,:,:,:,:) = 0.0
  select case (source)
  case(1)
    !WRITE(*,*) 'No source'
  case(2) ! sine wave (tend=1 or 10)
    call get_source(u_delta_quad,s)
    ! evaluate integral
    do icell=1,nx
      do jcell = 1,ny
        do i=1,m
          do j=1,m
            do intnode=1,m
              do jntnode=1,m
                source_vol(j,i,jcell,icell,1:nvar) = source_vol(j,i,jcell,icell,1:nvar) + &
                & 0.25*s(jntnode,intnode,jcell,icell,1:nvar)* &
                & legendre(x_quad(intnode),i-1)* &
                & w_x_quad(intnode)*&
                & legendre(y_quad(jntnode),j-1)* &
                & w_y_quad(jntnode)
              end do
            end do
          end do
        end do
      end do
    end do
  end select

  !========================
  ! Compute final DG update
  !========================
  ! Loop over cells
  dudt(:,:,:,:,1:nvar) = 0.0
  do icell=1,nx
    do jcell=1,ny
      ! Loop over modes
      do i=1,m
        do j=1,m
          dudt(j,i,jcell,icell,1:nvar) = &
          & 2*(oneoverdx*flux_vol1(j,i,jcell,icell,1:nvar) &
          & + oneoverdx*flux_vol2(j,i,jcell,icell,1:nvar) &
          &-oneoverdx*(edge(j,i,jcell,icell,1:nvar,1)&
          &-edge(j,i,jcell,icell,1:nvar,2)) &
          &-oneoverdx*(edge(j,i,jcell,icell,1:nvar,3)&
          &-edge(j,i,jcell,icell,1:nvar,4)) &
          & + source_vol(j,i,jcell,icell,1:nvar))
        end do
      end do
    end do
  end do
  !print*,'max dudt:',maxval(dudt)
  !print*,'min dudt:',minval(dudt)
  !dudt(:,:,:,:,4) = 0.0 ! block the update on pressure

end subroutine compute_update

subroutine apply_limiter(u)
  use parameters_dg_2d
  implicit none

  real(kind=8),dimension(1:m,1:m,1:ny,1:nx,1:nvar)::u

  if(use_limiter) then
    if (limiter_type == '1OR') then
      call compute_limiter(u)
    else if (limiter_type == 'HIO') then
      call high_order_limiter(u)
    else if (limiter_type=='LOW') then
      call limiter_low_order(u)

    else if (limiter_type=='POS') then
      call limiter_positivity(u)

    else if (limiter_type=='ROS') then
      call limiter_rossmanith(u)

    end if

  end if

end subroutine apply_limiter
