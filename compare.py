import numpy as np
#import matplotlib
#matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import math
import sys
import os

#shape = [int(sys.argv[2]),int(sys.argv[2])]
fig = plt.figure()
ax1 = plt.subplot2grid((3,2), (0,0), rowspan=3)
ax2 = plt.subplot2grid((3,2), (0,1))
ax3 = plt.subplot2grid((3,2), (1,1))
ax4 = plt.subplot2grid((3,2), (2,1))
var = sys.argv[1]
nx=256
ny=256
mx=2
my=2
nvar=4
size=nx*ny*my*mx*nvar
n=0
x = np.arange(ny)
y = np.arange(nx)
def visualize():
    global n
    try:
        m=np.fromfile("outputs/modes"+str(n).zfill(5)+".dat",dtype=np.float64,count=size).reshape(nvar,nx,ny,mx,my)
    except:
        print "Couldn't open it"
        return
    rho = m[0,nx/2,:,0,0]
    vx  = m[1,nx/2,:,0,0]/m[0,nx/2,:,0,0]
    vy  = m[2,nx/2,:,0,0]/m[0,nx/2,:,0,0]
    p = (5./3.-1.0)*(m[3,nx/2,:,0,0]-0.5*rho*(vx*vx+vy*vy));
    print str(n).zfill(5)
    im=(ax1.imshow((m[var,0:nx,0:ny,0,0]),interpolation="nearest",origin="lower",cmap="gnuplot"))
    ax2.cla()
    ax2.plot(x,rho,"b")
    ax2.set_xlim([0,nx])
    ax3.cla()
    ax3.plot(x,vx,"r")
    ax3.plot(x,vy,"g")
    ax3.set_xlim([0,nx])
    ax4.cla()
    ax4.plot(x,p,"m")
    ax4.set_xlim([0,nx])
    ax1.set_title("rho_min = "+str(np.min(m[0,:,:,0,0])))
    return im   
    
def change_file(event):
    sys.stdout.flush()
    global n,cbar
    if event.key=='right':
        n+=1
    elif event.key=='left':
        if n>0:
            n-=1
    else:
        return
    image=visualize()
    cbar.update_bruteforce(image)
    fig.canvas.draw_idle()

image=visualize()    
#cax = fig.make_axes(ax1, location="bottom", fraction=0.15, shrink=1.0, aspect=20)        
cbar=fig.colorbar(image,ax=ax1,orientation="horizontal")

fig.canvas.mpl_connect('key_press_event', change_file)   
plt.show()



